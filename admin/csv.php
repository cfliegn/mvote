<?php
error_reporting(0);
header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=export.csv");
header("Pragma: no-cache");
header("Expires: 0");
include("lib/config.php");


function convertToWindowsCharset($string) {
	$test = "";
	$charset = mb_detect_encoding($string, "UTF-8, ISO-8859-1, ISO-8859-15", true);
	$string = mb_convert_encoding($string, "Windows-1252", $charset);
	return $string;
}


// Parse ID
$id = hexdec($_GET['id']);
// parse id
if (!is_int($id))
	exit();
$dbh = new PDO(PDO, DBUSERNAME, DBPASSWORD);
$q = $dbh->prepare("SELECT * FROM mvote_umfrage WHERE id = :id");
$q->bindParam(":id", $id);
$q->execute();
$r = $q->fetch(PDO::FETCH_ASSOC);

print $r['titel']."\r\n\r\n";

$q2 = $dbh->prepare("SELECT * FROM mvote_frage WHERE umfrage_id = :umfrage_id");
$q2->bindParam(":umfrage_id", $id);
$q2->execute();

foreach($q2->fetchAll() as $row) {
	print $row['titel']."\r\n";
	if ($row['fragetyp'] != 2) {
		$antworten = explode("<br />", nl2br(trim($row['antworten'])));
		print "Durchlauf;Gesamt";
		foreach ($antworten as $a) {
			$a = trim(preg_replace('/\s+/', ' ', $a));
			if ($a != "")
				print ";".convertToWindowsCharset($a);
		}
		print "\r\n";
		$q_durchlauf = $dbh->prepare("SELECT *, (SELECT COUNT(*) FROM mvote_teilnahme WHERE durchlauf_id = mvote_durchlauf.id) as count FROM mvote_durchlauf WHERE frage_id = :frage_id");
		$q_durchlauf->bindParam(":frage_id", $row['id']);
		$q_durchlauf->execute();
		foreach($q_durchlauf->fetchAll() as $row_d) {
		
			print date("d.m.Y H:i",$row_d['start']).";". $row_d['count'];
		
			foreach ($antworten as $v=>$a) {
				if ($a != "") {
					$qa = $dbh->prepare("SELECT COUNT(*) as count FROM `mvote_durchlauf`, `mvote_teilnahme` WHERE mvote_durchlauf.id = :durchlauf_id AND mvote_durchlauf.id = mvote_teilnahme.durchlauf_id AND mvote_teilnahme.antwort = :antwort");
					$qa->bindParam(":durchlauf_id", $row_d['id']);
					$qa->bindParam(":antwort", $v);
					$qa->execute();
					$ra = $qa->fetch(PDO::FETCH_ASSOC);
			
					print ";".$ra['count'];
				}
			}
			
			print "\r\n";
		}
	} else {
		// Freitext
		print "Durchlauf;Woerter\r\n";
		// Lade Blackliste
		$q = $dbh->prepare("SELECT woerter FROM mvote_blackliste WHERE ersteller = :ersteller LIMIT 1");
		$q->bindParam(":ersteller", $_SESSION['userID']);
		$q->execute();
		$r = $q->fetch(PDO::FETCH_ASSOC);
	
		$blackliste = explode("<br />", nl2br(strtolower($r['woerter'])));
		
		// lade durchlauf ids
		$q_durchlauf = $dbh->prepare("SELECT *, (SELECT COUNT(*) FROM mvote_teilnahme WHERE durchlauf_id = mvote_durchlauf.id) as count FROM mvote_durchlauf WHERE frage_id = :frage_id");
		$q_durchlauf->bindParam(":frage_id", $row['id']);
		$q_durchlauf->execute();
		foreach($q_durchlauf->fetchAll() as $row_d) {
		
		
			$q = $dbh->prepare("SELECT antwort, count(*) as count FROM `mvote_teilnahme` WHERE durchlauf_id = :durchlauf_id GROUP by antwort");
			$q->bindParam(":durchlauf_id", $row_d['id']);
			$q->execute();
	
			$min = PHP_INT_MAX;
			$max = 0;
			$words = array();
			print date("d.m.Y H:i",$row_d['start']);
			foreach($q->fetchAll() as $row) {
				if (array_search(strtolower($row['antwort']), $blackliste) === false) {
					$words[$row['antwort']] = $row['count'];
					if ($row['count'] < $min)
						$min = $row['count'];
					if ($row['count'] > $max)
						$max = $row['count'];
				}
			}
	
			if (count($words) == 0) {
				$words["Es wurde kein Text eingegeben"] = 0;
				$max = 1;
				$min = 1;
			}
			foreach($words as $a=>$b) {
				print ";".convertToWindowsCharset($a)." (".$b.")";
			}
			
			print "\r\n";
			
		}	
	}
print "\r\n\r\n";
}
?>
