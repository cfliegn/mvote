<?php
error_reporting(E_ERROR);
foreach ($_GET as $a=>$b) {
	$_GET[$a] = htmlspecialchars($b);
}
foreach ($_POST as $a=>$b) {
	$_POST[$a] = htmlspecialchars($b);
}

$page = htmlspecialchars($_GET['p']);

include("lib/config.php");
include("lib/auth.php");

if (substr_count($page, '.') != 0 OR substr_count($page, '/') != 0 OR !file_exists("ajax/".$page.".php")) {
	header("HTTP/1.1 404 Not Found");
	print "Error 404";
	exit();
}
include("ajax/" . $page . ".php");
?>
