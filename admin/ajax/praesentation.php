<?php
/// AJAX ///////
// Parse ID
$id = hexdec($_GET['id']);
$durchlauf_id = (int)$_GET['did'];
// parse id
if (!is_int($id))
	exit();
$question = loadQuestion($id, $durchlauf_id);
if(isset($_GET['ajax'])){
print '<input type="hidden" id="t_a_r" value="'.$question->get_total().'"/>';
exit();}
if ($_GET['do'] == "stop") {
	$question->stop($_GET['did']);
	$question = loadQuestion($id, $durchlauf_id);
	print "<div class='messageBoxGreen'>Die Umfrage wurde gestoppt</div>";
	print "<h1>Frage: " . $question->getQuestion() . "</h1>";
	print $question->getPresentation();
	exit();
}
if ($_GET['do'] == "start") {
	if (!empty($_GET['e']) && $_GET['e'] > 0)
		$e = (int) $_GET['e'] + time();
	else
		$e = 0;
	if (!$question->isActive()) {
		$durchlauf_id = $question->start((int) $_GET['fid'], $e);
		$question = loadQuestion($id, $durchlauf_id);
		print "<h1>Frage: " . $question->getQuestion() . "</h1>";
		print $question->getAnswersHTMLForPresentation();
		print '<div id="did" style="display: none;">'.$durchlauf_id.'</div>';
	} else {
		print '<div class="messageBoxRed"><h1>Es müssen erst alle Durchläufe gestoppt werden, bevor ein neuer Durchlauf gestartet werden kann!</h1></div>';
	}
	exit();
}
if ($_GET['do'] == "showQuestion") {
	$question = loadQuestion($id, $durchlauf_id);
    print "<h1>Frage: " . $question->getQuestion() . "</h1>";
	print $question->getAnswersHTMLForPresentation();
	exit();
}
if ($_GET['do']== "deleteDurchlauf") {
    $q = $dbh->prepare("DELETE FROM `mvote_durchlauf` WHERE `id` = :id");
	$q->bindParam(":id", $durchlauf_id);
	$q->execute();
    
    print '<div class="messageBoxRed">Der Durchlauf wurde gelöscht!</h1>';
    
    exit();
}

if ($_GET['do'] == "showResults") {
	print "<h1>Frage: " . $question->getQuestion() . "</h1>";
	print $question->getPresentation();
	?>
	<script>
		var remainingTime = <?php print $question->getEnde() - time(); ?>;
		if (remainingTime > 0 && countdownActive == false) {
			if (remainingTime < 10) {
				countdownZeit = remainingTime;
			} else {
				countdownZeit = 10;
			}
			setTimeout('countdown(\'<?php print $question->getHexId(); ?>\',' + $("#did").val() + ', '+countdownZeit+')', 1000*(remainingTime-10));
		}
	</script><?php
	exit();
}

if ($_GET['do'] == "showQuestion") {
	print "<h1>Frage: " . $question->getQuestion() . "</h1>";
	print $question->getAnswersHTMLForPresentation();
	
}

if ($_GET['do'] == "showQR") {
	print "<h1>Frage: " . $question->getQuestion() . "</h1>";
	print '<img class="qrcode_max" src="qr.php?code=' . $question->getHexId() . '"> <div id="resizeControll"><a href="javascript:resizeQRplus();"><img title="Zoom In" src="images/IN.png"></a><a href="javascript:resizeQRminus();"><img title="Zoom Out" src="images/OUT.png"></a><br />
   <h2>' . SHORTROOTURL . $question->getHexId() . '</h2>';
	?><script>
		var remainingTime = <?php print $question->getEnde() - time(); ?>;
		if (remainingTime > 0 && countdownActive == false) {
			if (remainingTime < 10) {
				countdownZeit = remainingTime;
			} else {
				countdownZeit = 10;
			}
			setTimeout('countdown(\'<?php print $question->getHexId(); ?>\',' + $("#did").val() + ', '+countdownZeit+')', 1000*(remainingTime-10));
		}
	</script>
	
	<?php
}
if ($_GET['do'] == "sidebar") {
	$q = $dbh->prepare("SELECT * FROM mvote_umfrage WHERE id = :id");
	$q->bindParam(":id", $id);
	$q->execute();
	$r = $q->fetch(PDO::FETCH_ASSOC);
	$q2 = $dbh->prepare("SELECT * FROM mvote_frage WHERE umfrage_id = :umfrage_id ORDER by reihenfolge ASC");
	$q2->bindParam(":umfrage_id", $id);
	$q2->execute();
	foreach($q2->fetchAll() as $key=>$row) {
?>
		<div class="box"><br />
		<div style="float:left">Frage:&nbsp;<strong><?php print $row['frage'];?></strong></div>
        <div class="clear"></div>
        <?php if($row['fragetyp']==0){$type="Single Choice";}else if($row['fragetyp']==1){$type="Multiple Choice";}else{$type="Freitext";}?>
        <div style="float:left">Fragetyp:&nbsp;<strong><?php print $type;?></strong></div><br />	<br />
        <div class="clear"></div>
		<?php
		$q_durchlauf = $dbh->prepare("SELECT *, (SELECT COUNT(*) FROM mvote_teilnahme WHERE durchlauf_id = mvote_durchlauf.id) as count FROM mvote_durchlauf WHERE frage_id = :frage_id");
		$q_durchlauf->bindParam(":frage_id", $row['id']);
		$q_durchlauf->execute();
		$total_r=0;
		foreach($q_durchlauf->fetchAll() as $row_d1) {
			$total_r++;
		}
		
		$q_durchlauf = $dbh->prepare("SELECT *, (SELECT COUNT(*) FROM mvote_teilnahme WHERE durchlauf_id = mvote_durchlauf.id) as count FROM mvote_durchlauf WHERE frage_id = :frage_id");
		$q_durchlauf->bindParam(":frage_id", $row['id']);
		$q_durchlauf->execute();	
		$countDurchlaeufe = 0;
		
		foreach($q_durchlauf->fetchAll() as $row_d) {
			$countDurchlaeufe++;
			$count='';
			$lauft='';
		if ($row_d['ende'] == 0 || $row_d['ende'] > time()){
			$lauft='(Läuft)';
		}else{
			$count= $countDurchlaeufe.':';
		}?>
			<div class="col-lg-6 row"><?php print '<strong>'.$count.'</strong> Durchlauf: '.date("d.m.Y H:i",$row_d['start']).' Uhr'.$lauft; ?></div>
			<?php if ($row_d['ende'] > time()) { ?>
				(<strong id="sidebarCountDown<?php print $row_d['id']; ?>">?</strong>s)
	<script>
		
		for (var i = <?php print $row_d['ende'] - time(); ?>; i > 0; i--) {
			
			var timeoutTime = 1000*(<?php print $row_d['ende'] - time(); ?>-i);
			setTimeout('$("#sidebarCountDown<?php print $row_d['id']; ?>").html(' + i + ')',timeoutTime);
		}
	</script>
			<?php } ?>
			<div class="col-lg-6 row"><?php
			
			if($row['fragetyp']!=2){?>
            <a href="javascript:ladeAntworten('<?php print strtoupper(dechex($id)); ?>', <?php print $row_d['id']; ?>)"><img src="theme/icons/text_align_justify32.png" title="Antworten anzeigen"></a><?php
			}?>
			<a href="javascript:ladeErgebnisse('<?php print strtoupper(dechex($id)); ?>', <?php print $row_d['id']; ?>)"><img src="theme/icons/chart_bar32.png" title="Anzeigen"></a>
            <a onclick='return confirm("Diesen Durchlauf wirklich löschen?");' href="javascript:loescheDurchlauf('<?php print strtoupper(dechex($id)); ?>', <?php print $row_d['id']; ?>)"><img src="theme/icons/cross32.png" title="Löschen"></a>
		<?php if (!sizeof($row_d)){ ?><a href="javascript:start('<?php print strtoupper(dechex($id)); ?>', <?php print $row['fragetyp']; ?>, <?php print $row['id']; ?>, <?php print $row['zeit']; ?>)"><img src="theme/icons/reload.png"></a><?php
			}if($countDurchlaeufe==$total_r){
			if($row_d['ende'] == 0 || $row_d['ende'] > time()) {?>
			<a href="javascript:stop('<?php print strtoupper(dechex($id)); ?>', <?php print $row_d['id']; ?>)"><img src="theme/icons/stop32.png" title="Frage stoppen"></a>
			<?php }}?></div><div class="clear"></div><?php
		}
		?><div class="col-lg-6 row"><a href="javascript:start('<?php print strtoupper(dechex($id)); ?>', <?php print $row['fragetyp']; ?>, <?php print $row['id']; ?>, <?php print $row['zeit']; ?>)"  style="padding:15px 0"><img src="theme/icons/reload.png">Neuen Durchlauf starten</a></div>
		<?php
			
		if($type!='Freitext' && $countDurchlaeufe==$total_r && $total_r > 1){?>
            <div class="col-lg-6 row"><a href="?p=praesentation&id=<?php print strtoupper(dechex($id)); ?>&fid=<?php print $row['id']; ?>&do=compare" style="padding:15px 0"><img src="theme/icons/compare_32.png" title="Durchläufe vergleichen">Durchläufe vergleichen</a></div><?php }?><div class="clear"></div></div><?php
		}
		?>


</div><?php	
	}
if ($_GET['do'] == "sidebar1") {
	$q = $dbh->prepare("SELECT * FROM mvote_umfrage WHERE id = :id");
	$q->bindParam(":id", $id);
	$q->execute();
	$r = $q->fetch(PDO::FETCH_ASSOC);
	$q2 = $dbh->prepare("SELECT * FROM mvote_frage WHERE umfrage_id = :umfrage_id ORDER by reihenfolge ASC");
	$q2->bindParam(":umfrage_id", $id);
	$q2->execute();
	foreach($q2->fetchAll() as $key=>$row) {
	?>
		
		<?php
		$q_durchlauf = $dbh->prepare("SELECT *, (SELECT COUNT(*) FROM mvote_teilnahme WHERE durchlauf_id = mvote_durchlauf.id) as count FROM mvote_durchlauf WHERE frage_id = :frage_id");
		$q_durchlauf->bindParam(":frage_id", $row['id']);
		$q_durchlauf->execute();
		$countDurchlaeufe = 0;
		foreach($q_durchlauf->fetchAll() as $row_d) {
			$countDurchlaeufe++; } ?>
			<?php if ($countDurchlaeufe > 0) {
			if ($row_d['ende'] == 0 || $row_d['ende'] > time()) { ?>
				<a href="javascript:stop('<?php print strtoupper(dechex($id)); ?>', <?php print $row_d['id']; ?>)"><img src="theme/icons/stop.png" title="Frage stoppen"></a>
	<a href="javascript:ladeQR('<?php print strtoupper(dechex($id)); ?>', <?php print $row_d['id']; ?>)"><img src="images/View_QR_code.png" title="QR-Code"></a>			
        <?php if($row['fragetyp']!=2){?>
        <a href="javascript:ladeAntworten('<?php print strtoupper(dechex($id)); ?>', <?php print $row_d['id']; ?>)"><img src="theme/icons/text_align_justify.png" title="Antworten anzeigen"></a>
			<?php }
			?>    
			<a href="javascript:ladeErgebnisse('<?php print strtoupper(dechex($id)); ?>', <?php print $row_d['id']; ?>)"><img src="theme/icons/chart_bar.png" title="Anzeigen"></a>
		<?php /* f($row['fragetyp']!=2){?>
        <a href="?p=praesentation&id=<?php print strtoupper(dechex($id)); ?>&fid=<?php print $row['id']; ?>&do=compare"class="clear mar"><img src="theme/icons/compare.png" title="Durchläufe vergleichen"></a>
			<?php } */
			?>
<script>
        window.jQuery || document.write('<script src="lib/vendor/jquery-1.9.1.min.js"><\/script>')
</script>
        <script src="lib/vendor/jQuery.print.js"></script>
        <script type='text/javascript'>
                                    //<![CDATA[
                                    $(function() {
                                        $("#ele2").find('.print-link').on('click', function() {
                                            //Print ele2 with default options
                                            $.print("#ele2");
                                        });
                                        $("#ele4").find('button').on('click', function() {
                                            //Print ele4 with custom options
                                            $("#ele4").print({
                                                //Use Global styles
                                                globalStyles : false,
                                                //Add link with attrbute media=print
                                                mediaPrint : false,
                                                //Custom stylesheet
                                                stylesheet : "http://fonts.googleapis.com/css?family=Inconsolata",
                                                //Print in a hidden iframe
                                                iframe : false,
                                                //Don't print this
                                                noPrintSelector : ".avoid-this",
                                                //Add this at top
                                                prepend : "Hello World!!!<br/>",
                                                //Add this on bottom
                                                append : "<br/>Buh Bye!"
                                            });
                                        });
                                        // Fork https://github.com/sathvikp/jQuery.print for the full list of options
                                    });
                                    //]]>
        </script>
<?php
			}
		}
		}
		?>
        
        <?php if ($r['status'] == "true") { ?>
		<a target="_blank" href="#" onclick="return popitup('?p=comment&id=<?php print strtoupper(dechex($id)); ?>&do=comment')"><img src="theme/icons/comment.png" height="51" width="51" title="Kommentare"><div id="cmd_count">Keine</div></a>
		<input type="hidden" id="last" value=""/>
		<input type="hidden" id="last_r" value=""/>
		<input type="hidden" id="cmd_noti" value="-1" />
		<div id="results"></div>
	<?php } ?>
<script>
r1(<?php echo $id?>);
function r1(id){
    $.ajax({
 		 url: "ajax/comment_r.php?id="+id+"&last=1",
  		cache: false
	})
  	.done(function( html ) {
    	$( "#last" ).val(html);
		$("#cmd_count").html(html);$("#cmd_count").css('display','block');
  	});	
	setTimeout(function(){
         r1(id);
    },10000); // Adjust the timeout value as you like
}

<!--
function popitup(url) {
	newwindow=window.open(url,'name','height=700,width=600,scrollbars');
	if (window.focus) {newwindow.focus()}
	return false;
}
// -->
</script><?php
	}
	exit();
?>
