<!DOCTYPE html>
<html lang="de">
	<head>
		<meta charset="UTF-8">
		<script type="text/javascript" src="lib/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="lib/jquery-ui-1.9.2.js"></script>
		<script type="text/javascript" src="lib/mvote.js"></script>
		<link rel="stylesheet" href="theme/style.css" type="text/css" />
		<link rel="stylesheet" href="theme/mvote.css" type="text/css" />
		<script src="lib/Chart.min.js"></script>
        <title>mVote</title>
	</head>
	<?php if ($page == "Login") : ?><body onLoad="iframeLoaded()" onResize="iframeLoaded()"><?php else: ?><body><?php endif; ?>
		<div id="header"><img src="images/logo.jpg">
			<a id="titleText" href="?"><div style="float:left;padding:2% 0 0;width:10%;position:relative;">mVote</div></a>
			<?php 
				if ($_SESSION['isLoggedIn'] === true && $_SESSION['isLoggedIn'] !='' && $_GET['p']!='Login'){?>
			<a class="right" id="logoutLink" href="?p=Login">Logout</a><br><?php }?>
			<ul id="navi">
				<li><a href="?p=index">Umfragen</a></li>
				<li><a href="?p=settings">Einstellungen</a></li>
				<li><a href="http://www.uni-goettingen.de/de/529974.html" target="_blank">Hilfe / Anleitung</a></li>
			</ul>
				<?php
				if ($_SESSION['isLoggedIn'] === true && $page != "Login") {
					print '<span class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="float:right; text-align:right; padding:5px; background:#EEE">Eingeloggt als <strong>'.$_SESSION['user'].'</strong></span>';
				}
				?>
		</div>