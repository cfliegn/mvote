<?php
require_once './config.php';
if (isset($_POST["sub"])) {
  require_once "phpmailer/class.phpmailer.php";
  $name = trim($_POST["f_name"]);
  $l_name = trim($_POST["l_name"]);
  $pass = trim($_POST["pass1"]);
  $email = trim($_POST["uemail"]);
  $sql = "SELECT COUNT(*) AS count from `external_users` where email = :email_id";
  try {
    $stmt = $DB->prepare($sql);
    $stmt->bindValue(":email_id", $email);
    $stmt->execute();
    $result = $stmt->fetchAll();

    if ($result[0]["count"] > 0) {
      $msg = "Die E-Mail Adresse ist bereits im System vorhanden!";
      $msgType = "warning";
    } else {
      $sql = "INSERT INTO `external_users` (`f_name`,`l_name`, `pass`, `email`) VALUES " . "( :name,:l_name,:pass, :email)";
      $stmt = $DB->prepare($sql);
      $stmt->bindValue(":name", $name);
      $stmt->bindValue(":l_name", $l_name);
      $stmt->bindValue(":pass", md5($pass));
      $stmt->bindValue(":email", $email);
      $stmt->execute();
      $result = $stmt->rowCount();
      if ($result > 0) {
       
        $lastID = $DB->lastInsertId();
        $message = '<html><body>';
        $message .= '<h1>Sehr geehrter ' . $name . '!</h1>';
        $message .= '<p>Sie haben sich erfolgreich für mVote registriert. Um Ihre Registrierung abzuschließen, klicken Sie bitte auf den folgenden Link: <a href="'.SITE_URL.'activate.php?id=' . base64_encode($lastID) . '">Account aktivieren.</a>';
        $message .= "</body></html>";
        // php mailer code starts
        $mail = new PHPMailer(true);
        $mail->IsSMTP(); // telling the class to use SMTP

        $mail->SMTPDebug = 0;                     // enables SMTP debug information (for testing)
        $mail->SMTPAuth = true;                  // enable SMTP authentication
        $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
        $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
        $mail->Port = 465;                   // set the SMTP port for the GMAIL server

        $mail->Username = 'dummymvote@dummymail.com';
        $mail->Password = 'dummypassword';
	$mail->CharSet = 'UTF-8';
        $mail->SetFrom('dummymvote@dummymail.com', 'Mvote');
        $mail->AddAddress($email);

        $mail->Subject = trim("mVote Registrierung");
        $mail->MsgHTML($message);
		//Email Massage
		//echo $message;
        try {
          $mail->send();
          $msg = "Zur Bestätigung Ihrer E-Mail Adresse wurde Ihnen ein <strong>Freischaltungslink per E-Mail</strong> zugesendet. Bitte aktivieren Sie Ihren Account, indem Sie auf den Freischaltungslink klicken.";
          $msgType = "success";
        } catch (Exception $ex) {
          $msg = $ex->getMessage();
          $msgType = "warning";
        }
      } else {
        $msg = "Der Account konnte nicht angelegt werden.";
        $msgType = "warning";
      }
    }
  } catch (Exception $ex) {
    echo $ex->getMessage();
  }
}
include './header.php';
?>
<?php if ($msg <> "") { ?>
  <div class="alert alert-dismissable alert-<?php echo $msgType; ?>">
    <button data-dismiss="alert" class="close" type="button">x</button>
    <p><?php echo $msg; ?></p>
  </div>
<?php } ?>
<div class="container">
  <div class="row">
    <div class="col-lg-6">
      <div class="well contact-form-container">
        <form class="form-horizontal contactform" action="index.php" method="post" name="f" onsubmit="return validateForm();">
          <fieldset>
            <div class="form-group">
              <label class="col-lg-12 control-label" for="f_name">Vorname:
                <input type="text" placeholder="Ihr Vorname" id="f_name" class="form-control" name="f_name">
              </label>
            </div>
            
            <div class="form-group">
              <label class="col-lg-12 control-label" for="l_name">Nachname:
                <input type="text" placeholder="Ihr Nachname" id="l_name" class="form-control" name="l_name">
              </label>
            </div>

            <div class="form-group">
              <label class="col-lg-12 control-label" for="uemail">E-Mail Adresse:
                <input type="text" placeholder="Ihr E-mail" id="uemail" class="form-control" name="uemail">
              </label><?php 
			  		$q = "SELECT * FROM allowed_email_domains";
      				$q_d = $DB->prepare($q);
					$q_d->execute();
      				$result = $q_d->rowCount();
					if($result > 0){
						?>Erlaubte Domains:<?php
						$domain='';
						foreach($q_d->fetchAll() as $row) {
							print '<span>\''.$row['dom'].'\'</span> ';
							$domain.=$row['dom'].",";	
						}
					}
			?>
            </div>

            <div class="form-group">
              <label class="col-lg-12 control-label" for="pass1">Passwort:
                <input type="password" placeholder="Passwort" id="pass1" class="form-control" name="pass1">
              </label>
            </div>

            <div class="form-group">
              <label class="col-lg-12 control-label" for="pass1">Passwort (Wiederholung):
                <input type="password" placeholder="Passwort (Wiederholung)" id="pass2" class="form-control" name="pass2">
              </label>
            </div>

            <div style="height: 10px;clear: both"></div>

            <div class="form-group">
              <div class="col-lg-10">
                <button class="btn btn-primary" type="submit" name="sub">Absenden</button> 
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function domain(){
	email=$('#uemail').val();
	email=email.split('@');
	var nemail = [];
	for (var i = 0; i < email.length; i++) {
		if (email[i] !== "" && email[i] !== null) {
	   		  		nemail.push(email[i].trim());
	    		}
		}
		nemail=nemail[1];
		
	domains=('<?php echo $domain?>');
	domains=domains.split(',');
	var ndomains = [];
	for (var i = 0; i < domains.length; i++) {if (domains[i] !== "" && domains[i] !== null && domains[i]==nemail) return true;}
	return false;
}
  function validateForm() {
    var your_name = $.trim($("#f_name").val());
    var last_name = $.trim($("#l_name").val());
    var your_email = $.trim($("#uemail").val());
    var pass1 = $.trim($("#pass1").val());
    var pass2 = $.trim($("#pass2").val());


    // validate name
    if (your_name == "") {
      alert("Bitte geben Sie Ihren Vornamen ein.");
      $("#f_name").focus();
      return false;
    } else if (your_name.length < 3) {
      alert("Der Name muss mindestens 3 Zeichen lang sein.");
      $("#f_name").focus();
      return false;
    }
    
	// validate Last Name
    if (last_name == "") {
      alert("Bitte geben Sie Ihren Namen ein.");
      $("#l_name").focus();
      return false;
    } else if (last_name.length < 3) {
      alert("Der Name muss mindestens 3 Zeichen lang sein.");
      $("#l_name").focus();
      return false;
    }

    // validate email
    if (!isValidEmail(your_email)) {
      alert("Bitte geben Sie eine E-Mail Adresse ein.");
      $("#uemail").focus();
      return false;
    }
    // validate domain
    if (!domain()){
      alert("Ihre E-Mail Adresse ist nicht für die Nutzung von mVote freigeschaltet. Bitte wenden Sie sich an den Support des E-Learning-Services.");
      $("#uemail").focus();
      return false;
    }
	    // validate subject
    if (pass1 == "") {
      alert("Bitte geben Sie ein Passwort ein.");
      $("#pass1").focus();
      return false;
    } else if (pass1.length < 6) {
      alert("Das Passwort muss mindestens 6 Zeichen lang sein.");
      $("#pass1").focus();
      return false;
    }

    if (pass1 != pass2) {
      alert("Die eingegebenen Passwörter stimmen nicht überein.");
      $("#pass2").focus();
      return false;
    }

    return true;
  }

  function isValidEmail(email) {
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }


</script>

<?php
include './footer.php';
?>