<?php
/**************************** CONFIGURATION ****************************/
session_start();
define("DBUSERNAME", "mvoteuser");
define("DBPASSWORD", "dummy");
define("DBNAME", "mvote");
define("DBHOST", "localhost");
define("DBPORT", 3306);
define("PDO", "mysql:host=" . DBHOST . ";port=" . DBPORT . ";dbname=" . DBNAME);
define("ROOTURL", "https://mvote.anydomain.de/");
define("SHORTROOTURL", "http://mvote.anyshortdomain.de/");
define("TIMEZONE", "Europe/Berlin");
define("LDAPSERVER", "192.168.0.1");
define("LDAPRDN", "cn=0mvote,dc=general,dc=de");
define("LDAPPASSWORD", "dummypassword");
define("LDAPBASEDN", "ou=Customers,dc=de");
define("DEMOUSERNAME", "dummy");
define("DEMOPASSWORD", "demo");
define("DEMOUSERID", "demo");
define("DEMOUSERNAME2", "dummy2");
define("DEMOPASSWORD2", "demo2");
define("DEMOUSERID2", "demo2");
$domains=array();
$server_user=array();
/**************************** MISC ****************************/
date_default_timezone_set(TIMEZONE);

function getColor($c) {
	global $color;
	$colors = array('#FF4444', '#33B5E5','#FFBB33', '#9933CC', '#99CC00', '#FF0000', '#888800', '#669900', '#FF8800', '#af8bf1', '#8ba3f1'); // '#0099CC',
	$c++;
	return $colors[$c%9];
}


$dbh = new PDO(PDO, DBUSERNAME, DBPASSWORD);
function loadQuestion($umfrage_id, $durchlauf_id) {
	global $dbh;
	global $_GET;
	
	$q = $dbh->prepare("SELECT mvote_durchlauf.id as id, frage, mehrfachteilnahme, enthaltung, antworten, ende, antworten, fragetyp, diagrammtyp, (SELECT COUNT(*) FROM mvote_teilnahme WHERE durchlauf_id = mvote_durchlauf.id) as count FROM `mvote_frage`, `mvote_durchlauf` WHERE mvote_frage.umfrage_id = :id AND mvote_frage.id = mvote_durchlauf.frage_id AND mvote_durchlauf.id = :durchlauf_id");
	$q->bindParam(":id", $umfrage_id);
	$q->bindParam(":durchlauf_id", $durchlauf_id);
	$q->execute();
	$r = $q->fetch(PDO::FETCH_ASSOC);
	
	// create question class
	include_once("../app/questions/QTemplate.php");
	if ($durchlauf_id != 0 && $r['fragetyp'] == 0 || ($durchlauf_id == 0 && $_GET['ft'] == 0)) {
		include_once("../app/questions/SingleChoice.php");
		$question = new SingleChoice();
		
		$question->loadQuestion($umfrage_id, $r);
	} elseif ($r['fragetyp'] == 1 || ($durchlauf_id == 0 && $_GET['ft'] == 1)) {
		include_once("../app/questions/MultipleChoice.php");
		$question = new MultipleChoice();
		$question->loadQuestion($umfrage_id, $r);
	} elseif ($r['fragetyp'] == 2 || ($durchlauf_id == 0 && $_GET['ft'] == 2)) {
		include_once("../app/questions/FreeText.php");
		$question = new FreeText();
		$question->loadQuestion($umfrage_id, $r);
	}
	
	return $question;
}

function logEvent($obj, $message, $user, $isLoggedIn) {
    global $dbh;
    
    $q = $dbh->prepare("INSERT INTO `mvote_log_events` (`id`, `user`, `isLoggedIn`, `time`, `userAgent`, `obj`, `message`) VALUES (NULL, :user, :isLoggedIn, NULL, :userAgent, :obj, :message);");
    $q->bindParam(":user", $user);
    $q->bindParam(":isLoggedIn", $isLoggedIn);
    $userAgent = $_SERVER['HTTP_USER_AGENT'];
    $q->bindParam(":userAgent", $userAgent);
    
    $q->bindParam(":obj", $obj);
    $q->bindParam(":message", $message);
    
    $q->execute();
}
?>
