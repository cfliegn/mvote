$(document).ready(function() {
	
	$("#create-form").dialog({
		autoOpen: false,
		height: 300,
		width: "70%",
		buttons:{
			"Erstellen": function() {
				$("#create-form").find("form").submit();
			},
			Cancel: function() {
				$(this).dialog("close");
				$("#create-form").find("input").val("");
			}
		}
	});
	
	$("#create-frage").dialog({
		autoOpen: false,
		height: 600,
		width: "70%",
		buttons:{
			"Erstellen": function() {
				$("#create-frage").find("form").submit();
			},
			"Abbrechen": function() {
				$(this).dialog("close");
				$("#create-frage").find("input").val("");
			}
		}
	});
	
	$("#edit-frage").dialog({
		autoOpen: false,
		height: 600,
		width: "70%",
		buttons:{
			"Speichern": function() {
				$("#edit-frage").find("form").submit();
			},
			"Abbrechen": function() {
				$(this).dialog("close");
				$("#edit-frage").find("input").val("");
			}
		}
	});
	
	// hide useless inputs
	$("#edit-frage #fragetyp").change(function() {
		hideInputIfNeeded("#edit-frage");
	});
	$("#create-frage #fragetyp").change(function() {
		hideInputIfNeeded("#create-frage");
	});
    
    $("#create-frage #fragetyp").change(function() {
		if ($("#create-frage #fragetyp").val() != 1) {
            $("#create-frage #showEnthaltung").hide();
            $("#create-frage #showEnthaltung input").prop('checked', false);
        } else {
            $("#create-frage #showEnthaltung").show();
        }
	});
    
    $("#edit-frage #fragetyp").change(function() {
		if ($("#edit-frage #fragetyp").val() != 1) {
            $("#edit-frage #showEnthaltung").hide();
            $("#edit-frage #showEnthaltung input").prop('checked', false);
        } else {
            $("#edit-frage #showEnthaltung").show();
        }
	});
	
	
});

function hideInputIfNeeded(div) {
	if ($(div + " #fragetyp").val() == 2) {
		$(div + " .noFreitext").hide();
	} else {
		$(div + " .noFreitext").show();
	}
}

function zeichneBalkendiagramm(canvasName, data) {
	// Farbdefinition
	var balkenColor = ['#33B5E5','#FFBB33', '#99CC00', '#FF4444', '#0099CC', '#9933CC', '#669900', '#FF8800', '#888800'];
	
	// Maximum
	var max = -1000;
	$.each(data.values, function(row, value) {
		if (max < value[1])
			max = value[1]
	});
	
	var width = $("#" + canvasName).width();
	var height = $("#" + canvasName).height();
	var marginLeft = 20;
	var marginBottom = 50;
	var rotateAngle = 30;
	var canvas = document.getElementById(canvasName);
	var ctx = canvas.getContext('2d');
	
	ctx.beginPath();
	ctx.strokeStyle = "darkgray";
	ctx.font = '30px Arial';
	// Y-Achse
	ctx.moveTo(marginLeft,00);
	ctx.lineTo(marginLeft,height-marginBottom);
	// X-Achse
	ctx.lineTo(width,height-marginBottom);
	// Scala Y-Achse
	ctx.fillText("0", 0, height-marginBottom+10);
	ctx.fillText(max, 0, 20);
	ctx.stroke();

	// Balken zeichnen
	$.each(data.values, function(row, value) {
		// BalkenhÃ¶he:
		var balkenHeight = (height-marginBottom - 20)/max*value[1];
		// Verschiebung:
		var platzProBalken = (width - marginLeft * 2) / data.values.length;
		var verschiebung = platzProBalken * row + platzProBalken/2;
		// Farbe setzen
		ctx.fillStyle = balkenColor[row%balkenColor.length];
		// Verschiebung X, , Breite
		ctx.fillRect(verschiebung,height-marginBottom,platzProBalken*2/3,-balkenHeight);
		
		// Beschriftung
		ctx.beginPath();
		ctx.save();
		ctx.translate(verschiebung + platzProBalken*2/3/2,height-marginBottom);
		ctx.rotate(Math.PI*2/360*rotateAngle);
		ctx.fillStyle = "black";
		ctx.textAlign = "center";
		ctx.fillText(value[0], 0, 00);
		ctx.stroke();
		ctx.restore();
	});
}

function zeichneKreisdiagramm(canvasName, data) {
	
	// Farbdefinition
	var balkenColor = ['#33B5E5','#FFBB33', '#99CC00', '#FF4444', '#0099CC', '#9933CC', '#669900', '#FF8800', '#888800'];
	
	// Summe aller Werte
	var sum = 0;
	$.each(data.values, function(row, value) {
		sum += value[1];
	});
	
	var width = $("#" + canvasName).width();
	var height = $("#" + canvasName).height();
	var margin = 20;
	var canvas = document.getElementById(canvasName);
	var ctx = canvas.getContext('2d');	
	
	var radius = 0;
	if (width < height)
		radius = width/2-margin;
	else
		radius = height/2-margin
	
	var lastAngle = 0;
	$.each(data.values, function(row, value) {
		var winkel = value[1]/sum*360;

		// Zeichne
		ctx.beginPath();
		ctx.moveTo(width/2, height/2);
		var angleStart = Math.PI*2/360*lastAngle;
		var angleEnd = Math.PI*2/360*(lastAngle + winkel);
		
		
		ctx.arc(width/2, height/2, radius, angleStart, angleEnd, false);
		ctx.fillStyle = balkenColor[row%balkenColor.length];
		ctx.fill();
		
		// Beschriftung
		
		// Verschiebung Horizonal
		var verschiebungHorizontal =  Math.cos(Math.PI*2/360*((lastAngle + winkel -winkel/2))) * radius;
		var verschiebungVertikal =  Math.sin(Math.PI*2/360*((lastAngle + winkel -winkel/2))) * radius;
		
		ctx.beginPath();
		ctx.save();
		ctx.translate(width/2 + verschiebungHorizontal, height/2 + verschiebungVertikal);
		ctx.fillStyle = "black";
		ctx.textAlign = "center";
		ctx.font = '30px Arial';
		if (value[1] != 0) {
			ctx.fillText(value[0], 0, 00);
		}
		ctx.stroke();
		ctx.restore();
		
		lastAngle += winkel;
	});
}

function zeichneWordCloud() {
}

// Presentation
function start(umfrage_id, frage_typ, frage_id, zeit) {
	$("#body").html("<h1>Lade Frage ...</h1>");
	$('#body').load('ajax.php?p=praesentation&id=' + umfrage_id + '&ft=' + frage_typ + '&fid=' + frage_id + '&e=' + zeit + '&do=start', function() {
		if (zeit > 0) {
			if (zeit < 10) {
				countdownZeit = zeit;
			} else {
				countdownZeit = 10;
			}
			setTimeout('countdown(\'' + umfrage_id + '\',' + $("#did").text() + ', '+countdownZeit+')', 1000*(zeit-10));
		}
		ladeSidebar(umfrage_id);
		ladeSidebar1(umfrage_id);
	});
}

function stop(umfrage_id, durchlauf_id) {
	$("#body").html("<h1>Stoppe Frage ...</h1>");
	$('#body').load('ajax.php?p=praesentation&id=' + umfrage_id + '&did=' + durchlauf_id + '&do=stop', function() {
		ladeSidebar(umfrage_id);
		ladeSidebar1(umfrage_id);
	});
	countdownActive = false;
}

function ladeQR(umfrage_id, durchlauf_id) {
	$('#body').load('ajax.php?p=praesentation&id=' + umfrage_id + '&did=' + durchlauf_id + '&do=showQR');
}

function ladeAntworten(umfrage_id, durchlauf_id) {
	$('#body').load('ajax.php?p=praesentation&id=' + umfrage_id + '&did=' + durchlauf_id + '&do=showQuestion');
}


function ladeFrage(umfrage_id, durchlauf_id) {
	$('#body').load('ajax.php?p=praesentation&id=' + umfrage_id + '&did=' + durchlauf_id + '&do=showQuestion');
}

function ladeErgebnisse(umfrage_id, durchlauf_id) {
	$('#body').load('ajax.php?p=praesentation&id=' + umfrage_id + '&did=' + durchlauf_id + '&do=showResults');
}
function ladeErgebnisse1(umfrage_id, durchlauf_id) {
	$('#total_ans').load('ajax.php?p=praesentation&id=' + umfrage_id + '&did=' + durchlauf_id + '&do=showResults&ajax=1')
  	.done(function( html ) {
    	$( "#last" ).val(html);
		$("#t_a").val($("#t_a_").val());
				
  	});	;
}

function loescheDurchlauf(umfrage_id, durchlauf_id) {
	$('#body').load('ajax.php?p=praesentation&id=' + umfrage_id + '&did=' + durchlauf_id + '&do=deleteDurchlauf', function() {
		ladeSidebar(umfrage_id);
		ladeSidebar1(umfrage_id);
	});
}

var countdownActive = false;
function countdown(umfrage_id, durchlauf_id, zeit) {
	console.log("starting countdown (" + durchlauf_id + ")");
	countdownActive = true;
	for (var i = zeit; i > 0; i--) {
		setTimeout('$("#body h1").html("<h1>Noch ' + i + ' Sekunden</h1>")',1000*(zeit-i));
	}
	
	setTimeout('stop(\'' + umfrage_id + '\',' + durchlauf_id + ')', 1000*zeit);
}

function ladeSidebar(umfrage_id) {
	$('#sidebar').load('ajax.php?p=praesentation&id=' + umfrage_id + '&do=sidebar');
}
function ladeSidebar1(umfrage_id) {
	$('#sidebar1').empty();
	$('#sidebar1').load('ajax.php?p=praesentation&id=' + umfrage_id + '&do=sidebar1');
}
function resizeQRplus() {
	$("img.qrcode_max").width($("img.qrcode_max").width()+20 + "px")
}

function resizeQRminus() {
	$("img.qrcode_max").width($("img.qrcode_max").width()-20 + "px")
}


