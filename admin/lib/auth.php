<?php
error_reporting(E_ERROR);
$localUsers = array(
    'user101' => 'dummy1',
    'user102' => 'dummy2'

);
if ($_POST['user'] != DEMOUSERNAME){
	if(!empty($_POST['user']) && !empty($_POST['passwort'])){
		$l = $dbh->prepare("SELECT * from `external_users` where `email` = :email AND `pass`=:pass AND `status` = 'approved';");
		$l->bindParam(":email", $_POST['user']);
		$l->bindParam(":pass", md5($_POST['passwort']));
		$l->execute();
		$m = $l->fetch(PDO::FETCH_ASSOC);		
		if(is_array($m) && in_array($_POST['user'],$m)){
				$_SESSION['isLoggedIn'] = true;
				$_SESSION['user'] = $_SESSION['userID'] = $_POST['user'];
				$_SESSION['allowedQuestions'] = array(); // Dieses Array wird beim Aufruf der Startseite mit allen erlaubten Umfrage-Ids gefüllt.
				$successMsg = "Login mit lokalem Nutzer erfolgreich.";
				$logMsg = "Lokaler Login (" . $_SESSION['user'] . ") erfolgreich";
		}
	}
}
if ($_SESSION['isLoggedIn'] !== true) {

	if (!empty($_POST['user']) && !empty($_POST['passwort'])) {
		
		if (preg_match('/[^a-zA-Z\d.-]/i', $_POST['user'])) {
			$errorMsg = "Benutzername enthält nicht erlaubte Zeichen.";
			$logMsg = "Benutzername: nicht erlaubte Zeichen";
		}
		// demo user
		elseif ($_POST['user'] == DEMOUSERNAME && $_POST['passwort'] == DEMOPASSWORD){
			$_SESSION['isLoggedIn'] = true;
			$_SESSION['user'] = ucwords(DEMOUSERNAME);
			$_SESSION['userID'] = DEMOUSERID;
			$_SESSION['allowedQuestions'] = array(); // Dieses Array wird beim Aufruf der Startseite mit allen erlaubten Umfrage-Ids gefüllt.
			$successMsg = "Demo-Login erfolgreich.";
			$logMsg = "Lokaler Login (" . DEMOUSERNAME . ") erfolgreich";
		}
		// demo user 2
		elseif ($_POST['user'] == DEMOUSERNAME2 && $_POST['passwort'] == DEMOPASSWORD2) {
			$_SESSION['isLoggedIn'] = true;
			$_SESSION['user'] = ucwords(DEMOUSERNAME2);
			$_SESSION['userID'] = DEMOUSERID2;
			$_SESSION['allowedQuestions'] = array(); // Dieses Array wird beim Aufruf der Startseite mit allen erlaubten Umfrage-Ids gefüllt.
			$successMsg = "Demo2-Login erfolgreich.";
			$logMsg = "Lokaler Login (" . DEMOUSERNAME2 . ") erfolgreich";
		}
        // localuser array
        elseif (!empty($localUsers[$_POST['user']]) && $localUsers[$_POST['user']] == $_POST['passwort']) {
            $_SESSION['isLoggedIn'] = true;
			$_SESSION['user'] = ucwords($_POST['user']);
			$_SESSION['userID'] = $_POST['user'];
			$_SESSION['allowedQuestions'] = array(); // Dieses Array wird beim Aufruf der Startseite mit allen erlaubten Umfrage-Ids gefüllt.
			$successMsg = "Login mit lokalem Nutzer erfolgreich.";
			$logMsg = "Lokaler Login (" . $_SESSION['user'] . ") erfolgreich";
        }
		// ldap login
		else {
			$ldapconn = ldap_connect(LDAPSERVER);
			
			// LDAP Server erreichbar
			if ($ldapconn) {
				$ldapbind = ldap_bind($ldapconn, LDAPRDN, LDAPPASSWORD);
				if (!$ldapbind) {
					$errorMsg = "Der Loginserver ist nicht richtig konfiguriert!";
					$logMsg = "LDAP Server: falsch konfiguriert";
				} else {
					// Testen ob ein passender Benutzer existiert
					$results = ldap_search($ldapconn, LDAPBASEDN, "cn=" . $_POST['user']);
					$entries = ldap_get_entries($ldapconn, $results);
					// Keiner oder mehr als ein Benutzer gefunden.
					if ($entries['count'] != 1) {
						$errorMsg = "Der eingegebene Benutzername stimmt nicht.";
						$logMsg = "Benutzername: falsch/nicht eindeutig: " . $_POST['user'];
					} else {
						$dn = $entries[0]['dn'];
						//exit("An den magic_quotes_workaround gedacht?!");
						// Magic quotes workaround: $ldapbind = ldap_bind($ldapconn, $dn, str_replace('\"', '"', html_entity_decode($_POST['passwort'])));
						$ldapbind = ldap_bind($ldapconn, $dn, stripslashes(html_entity_decode($_POST['passwort'])));
						if ($ldapbind) {
							$_SESSION['isLoggedIn'] = true;
							$_SESSION['userID'] = $entries[0]['uid'][0];
							$_SESSION['user'] = $entries[0]['cn'][0];
							$_SESSION['allowedQuestions'] = array(); // Dieses Array wird beim Aufruf der Startseite mit allen erlaubten Umfrage-Ids gefüllt.
							$successMsg = "Login erfolgreich.";
							$logMsg = "LDAP Login erfolgreich";
						} else {
							$_SESSION['isLoggedIn'] = false;
							$errorMsg = "Die Zugangsdaten stimmen nicht überein!";
							$logMsg = "Password falsch (Username: " . $dn . ", Error: " . ldap_error($ldapconn) . ")";
						}
					}
				}
				
			}
			// LDAP Server nicht erreichbar
			else {
				$errorMsg = "Der Loginserver ist nicht erreichbar. Bitte versuchen Sie es später wieder!";
				$logMsg = "LDAP Verbindung nicht möglich";
			}
		}
	}
}

if ($_SESSION['isLoggedIn'] !== true && $page != "faq") {
	$page = "Login";
}

if (!empty($_GET['id']) && array_search(hexdec($_GET['id']), $_SESSION['allowedQuestions']) === false) {
	$page = "Login";
}

if (!empty($logMsg)) {
    logEvent($page, $logMsg, $_SESSION['user'], $_SESSION['isLoggedIn']);
}
?>
