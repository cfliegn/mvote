<?php
$dbh = new PDO(PDO, DBUSERNAME, DBPASSWORD);
// parse id
$id = hexdec($_GET['id']);
if (!is_int($id))
	exit();

if ($_GET['do'] == "new") {
	$q = $dbh->prepare("INSERT INTO mvote_umfrage (`titel`, `ersteller`) VALUES (:titel, :ersteller)");
	$q->bindParam(":titel", $_POST['titel']);
	$q->bindParam(":ersteller", $_SESSION['userID']);
	$q->execute();
} elseif ($_GET['do'] == "del") {

		
	$q = $dbh->prepare("DELETE FROM mvote_umfrage WHERE `id` = :id");
	$q->bindParam(":id", $id);
	$q->execute();
} elseif ($_GET['do'] == "reset") {
		
	$q = $dbh->prepare("DELETE FROM `mvote_durchlauf` WHERE `frage_id` IN (SELECT id FROM `mvote_frage` WHERE umfrage_id = :id)");
	$q->bindParam(":id", $id);
	$q->execute();
} elseif ($_GET['do'] == "copy") {
	
	$q1 = $dbh->prepare("SELECT * FROM mvote_umfrage WHERE id = :id");
	$q1->bindParam(":id", $id);
	$q1->execute();
	$r1 = $q1->fetch(PDO::FETCH_ASSOC);
	
	// Umfrage kopieren
	$q = $dbh->prepare("INSERT INTO mvote_umfrage (`titel`, `ersteller`, `countdown`, `status`) VALUES (:titel, :ersteller, :countdown, :status)");
	$titelTmp = $r1['titel'] . " (Kopie " .date("d.m.Y H:i"). ")";
	$q->bindParam(":titel", $titelTmp);
	$q->bindParam(":ersteller", $_SESSION['userID']);
	$q->bindParam(":countdown", $r1['countdown']);
	$q->bindParam(":status", $r1['status']);
	$q->execute();
	
	// Neue ID bestimmen
	$q3 = $dbh->prepare("SELECT * FROM mvote_umfrage WHERE titel = :titel AND ersteller = :ersteller ORDER BY `mvote_umfrage`.`id` DESC LIMIT 1");
	$q3->bindParam(":titel", $titelTmp);
	$q3->bindParam(":ersteller", $_SESSION['userID']);
	$q3->execute();
	$r3 = $q3->fetch(PDO::FETCH_ASSOC);
	$id_new = $r3['id'];
	
	// Fragen kopieren
	$q2 = $dbh->prepare("SELECT * FROM mvote_frage WHERE umfrage_id = :umfrage_id");
	$q2->bindParam(":umfrage_id", $id);
	$q2->execute();
	
	foreach($q2->fetchAll() as $row) {
		$q = $dbh->prepare("INSERT INTO `mvote_frage` (`id`, `titel`, `frage`, `erklaerung`, `zeit`, `reihenfolge`, `fragetyp`, `diagrammtyp`, `mehrfachteilnahme`, `antworten`, `r_antworten`, `umfrage_id`) VALUES (NULL, :titel, :frage, :erklaerung, :zeit, '0', :fragetyp, :diagrammtyp, :mehrfachteilnahme, :antworten, :r_antworten, :umfrage_id);");
		$q->bindParam(":titel", $row['titel']);
		$q->bindParam(":frage", $row['frage']);
		$q->bindParam(":erklaerung", $row['erklaerung']);
		$q->bindParam(":zeit", $row['zeit']);
		$q->bindParam(":fragetyp", $row['fragetyp']);
		$q->bindParam(":diagrammtyp", $row['diagrammtyp']);
		$q->bindParam(":mehrfachteilnahme", $row['mehrfachteilnahme']);
		$q->bindParam(":antworten", $row['antworten']);
		$q->bindParam(":r_antworten", $row['r_antworten']);
		$q->bindParam(":umfrage_id", $id_new);
		$q->execute();;
	}
}
?>
<div id="body">
	<h1>Umfragen</h1>
	<h2>Eigene Umfragen<span class="right" style="margin-top: -5px;"><button onclick='$("#create-form").dialog("open");'><img src="theme/icons/add.png" height="15" width="15"> neue Umfrage erstellen</button></span></h2>
	<table id="umfrageListe">
		<thead>
			<tr>
				<td style="width: 10px;">ID</td>
				<td>Umfrage</td>
				<td style="width: 50px;">Teilnehmer</td>
				<td style="width: 50px;">Status</td>
				<td style="width: 250px;">Optionen</td>
			</tr>
		</thead>
		
		<tbody>
		<?php
		$q = $dbh->prepare("SELECT *, (SELECT COUNT(*) FROM mvote_frage, mvote_durchlauf, mvote_teilnahme
	WHERE
		
        	mvote_umfrage.id = mvote_frage.umfrage_id AND
        	mvote_frage.id = mvote_durchlauf.frage_id AND
                mvote_durchlauf.id = mvote_teilnahme.durchlauf_id) as count, (SELECT COUNT(*) FROM mvote_frage, mvote_durchlauf WHERE mvote_frage.umfrage_id = mvote_umfrage.id AND mvote_frage.id = mvote_durchlauf.frage_id AND (mvote_durchlauf.ende = 0 || mvote_durchlauf.ende > :ende)) as status, (SELECT mvote_durchlauf.id FROM mvote_frage, mvote_durchlauf WHERE mvote_frage.umfrage_id = mvote_umfrage.id AND mvote_frage.id = mvote_durchlauf.frage_id AND (mvote_durchlauf.ende = 0 || mvote_durchlauf.ende > 1458294832) LIMIT 1) as did FROM mvote_umfrage WHERE mvote_umfrage.ersteller = :ersteller");
                $time = time();
		$q->bindParam(":ende", $time);
		$q->bindParam(":ersteller", $_SESSION['userID']);
		$q->execute();
		
		foreach($q->fetchAll() as $row) {
		
		$_SESSION['allowedQuestions'][] = $row['id'];
		?>
			<tr>
				<td style="width: 10px;">#<?php print strtoupper(dechex($row['id'])); ?></td>
				<td><a href="?p=praesentation&id=<?php print strtoupper(dechex($row['id'])); ?>&did=<?= $row['did']; ?>"><?php print $row['titel']; ?></a></td>
				<td style="width: 50px; text-align: center;"><?php print $row['count']; ?></td>
				<td style="width: 50px; text-align: center;"><?php if ($row['status'] == 0) print 'Gestoppt'; else print 'Gestartet'; ?></td>
				<td style="width: 250px; text-align: center;">
					<a href="?p=praesentation&id=<?php print strtoupper(dechex($row['id'])); ?>&did=<?= $row['did']; ?>"><img src="theme/icons/qr_icon.png" title="Präsentationsansicht"></a>
					<a href="?p=editUmfrage&id=<?php print strtoupper(dechex($row['id'])); ?>"><img src="theme/icons/pencil32.png" title="Bearbeiten"></a>
					<a onclick='return confirm("Diese Umfrage wirklich zurücksetzen?");' href="?p=index&id=<?php print strtoupper(dechex($row['id'])); ?>&do=reset"><img src="theme/icons/arrow_undo.png" title="Zurücksetzen"></a>
					<a href="csv.php?id=<?php print strtoupper(dechex($row['id'])); ?>"><img src="theme/icons/disk.png" title="Exportieren"></a>
					<a onclick='return confirm("Diese Umfrage wirklich kopieren?");' href="?p=index&id=<?php print strtoupper(dechex($row['id'])); ?>&do=copy"><img src="theme/icons/page_copy.png" title="Kopieren"></a>
					<a onclick='return confirm("Diese Umfrage wirklich löschen?");' href="?p=index&id=<?php print strtoupper(dechex($row['id'])); ?>&do=del"><img src="theme/icons/cross32.png" title="Löschen"></a>
                    
					<a target="_blank" href="?p=comment&id=<?php print strtoupper(dechex($row['id'])); ?>&do=comment"><img src="theme/icons/comment.png" height="25" width="25" title="Kommentare anzeigen" onclick="return popitup('?p=comment&id=<?php print strtoupper(dechex($row['id'])); ?>&do=comment')"></a>
                    <script language="javascript" type="text/javascript">
<!--
function popitup(url) {
	newwindow=window.open(url,'name','height=700,width=600,scrollbars');
	if (window.focus) {newwindow.focus()}
	return false;
}

// -->
</script>
                    </td>
			</tr>
		<?php
		}
		?>
		</tbody>
	</table>
	
	<!-- Freigaben -->
	<h2>Freigaben</h2>
	<table id="umfrageListe">
		<thead>
			<tr>
				<td style="width: 10px;">ID</td>
				<td>Umfrage</td>
				<td style="width: 50px;">Teilnehmer</td>
				<td style="width: 50px;">Status</td>
				<td style="width: 250px;">Optionen</td>
			</tr>
		</thead>
		<tbody>
		<?php
		
		
		
		$q = $dbh->prepare("SELECT *, (SELECT COUNT(*) FROM mvote_frage, mvote_durchlauf, mvote_teilnahme
	WHERE
		
        	mvote_umfrage.id = mvote_frage.umfrage_id AND
        	mvote_frage.id = mvote_durchlauf.frage_id AND
                mvote_durchlauf.id = mvote_teilnahme.durchlauf_id) as count, (SELECT COUNT(*) FROM mvote_frage, mvote_durchlauf WHERE mvote_frage.umfrage_id = mvote_umfrage.id AND mvote_frage.id = mvote_durchlauf.frage_id AND (mvote_durchlauf.ende = 0 || mvote_durchlauf.ende > :ende)) as status FROM mvote_umfrage WHERE mvote_umfrage.ersteller != :ersteller AND (mvote_umfrage.freigabe LIKE '%".strtolower($_SESSION['user'])."%' || mvote_umfrage.freigabe LIKE '%".strtolower($_SESSION['userID'])."%')");
                $time = time();
		$q->bindParam(":ende", $time);
		$q->bindParam(":ersteller", $_SESSION['userID']);
		$q->execute();
		foreach($q->fetchAll() as $row) {
			$_SESSION['allowedQuestions'][] = $row['id'];
		?>
			<tr>
				<td style="width: 10px;">#<?php print strtoupper(dechex($row['id'])); ?></td>
				<td><a href="?p=praesentation&id=<?php print strtoupper(dechex($row['id'])); ?>"><?php print $row['titel']; ?></a>
</td>
				<td style="width: 50px; text-align: center;"><?php print $row['count']; ?></td>
				<td style="width: 50px; text-align: center;"><?php if ($row['status'] == 0) print '<img src="theme/icons/stop32.png" title="Läuft nicht">'; else print '<img src="theme/icons/control_play_blue.png" title="Läuft">'; ?></td>
				<td style="width: 250px; text-align: center;">
					<a href="?p=praesentation&id=<?php print strtoupper(dechex($row['id'])); ?>"><img src="theme/icons/qr_icon.png" title="Präsentationsansicht"></a>
					<a href="?p=editUmfrage&id=<?php print strtoupper(dechex($row['id'])); ?>"><img src="theme/icons/pencil32.png" title="Bearbeiten"></a>
					<a onclick='return confirm("Diese Umfrage wirklich zurücksetzen?");' href="?p=index&id=<?php print strtoupper(dechex($row['id'])); ?>&do=reset"><img src="theme/icons/arrow_undo.png" title="Zurücksetzen"></a>
					<a href="csv.php?id=<?php print strtoupper(dechex($row['id'])); ?>"><img src="theme/icons/disk.png" title="Exportieren"></a>
					<a onclick='return confirm("Diese Umfrage wirklich kopieren?");' href="?p=index&id=<?php print strtoupper(dechex($row['id'])); ?>&do=copy"><img src="theme/icons/page_copy.png" title="Kopieren"></a>
					<a onclick='return confirm("Diese Umfrage wirklich löschen?");' href="?p=index&id=<?php print strtoupper(dechex($row['id'])); ?>&do=del"><img src="theme/icons/cross32.png" title="Löschen"></a></td>
			</tr>
		<?php
		}
		?>
		</tbody>
	</table>
	<div id="create-form" title="Neue Umfrage erstellen">
		<form action="?do=new" method="post">
			<label class="col-xs-2">Titel:</label> <input type="text" name="titel" value="" class="col-xs-9">
		</form>
	</div>
</div>
