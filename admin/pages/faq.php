<div id="body" class="faq">
	<h1>Hilfe</h1>
    
    <p>Eine Einführung in Clicker-Systeme wie mVote, mitsamer einer Auflistung möglicher Einsatzszenarien, steht als <a href="files/Clicker-Hilfestellung.pdf">.pdf-Datei zum Download bereit</a>. Weiterhin wird im Folgenden die Nutzung von mVote beschrieben:</p>
    
<ol>
	<li><a href="#Login">Login</a></li>
	<li><a href="#Hauptmenü">Hauptmenü</a><br>
	<li><a href="#NeueUmfrageerstellen">Neue Umfrage erstellen</a></li>
	<li><a href="#Freigeben">Freigeben </a></li>
	<li><a href="#Fragen hinzufügen">Fragen hinzufügen</a></li>
	<li><a href="#Fragetypen">Fragetypen</a></li>
	<li><a href="#Blacklist">Blacklist </a></li>
	<li><a href="#Darstellung">Darstellung </a></li>
	<li><a href="#Präsentationsansicht">Präsentationsansicht</a></li>
	<li><a href="#NeuenDurchlaufstarten">Neuen Durchlauf starten</a></li>
	<li><a href="#QRCoderichtiganzeigen">QR Code richtig anzeigen</a></li>
	<li><a href="#Durchlaufstoppen">Durchlauf stoppen</a></li>
	<li><a href="#Durchläufevergleichen">Durchläufe vergleichen</a></li>
	<li><a href="#WeitereOptionen">Weitere Optionen</a></li>
</ol>
	<h2><a name="Login">1. Login</a></h2>
<p>Hier bitte mit GWDG-Account und Passwort anmelden.</p>
<img src="images/faq/login.png" style="float: left; width: 500px;">
<div style="clear: both; "/>


<h2><a name="Hauptmenü">2. Hauptmenü</a></h2>
<p>Das ist das Hauptmenü in dem Sie zunächst starten und Ihre Umfragen verwalten können.</p>
<p>Unter "Neue Umfrage erstellen" können alle Umfragetypen erstellt werden.</p>
<img src="images/faq/main.png" style="float: left; width: 800px;">
<div style="clear: both; "/>
<p>Um Umfragen zu bearbeiten oder <a href="#Fragen hinzufügen">um Fragen hinzuzufügen</a> muss zunächst der "Stift" angeklickt werden.</p>
<div style="clear: both; "/>

<h2><a name="NeueUmfrageerstellen">3. Neue Umfrage erstellen</a></h2>
<p>Im folgendem Fenster kann der nun der Titel festgelegt werden.</p>

<img src="images/faq/neuer_titel.png" style="float: ;width: 550px;"> <br>
<div style="clear: both; "/>

<p> Im Bearbeitungsmodus gelangt man auf Übersichtsseite auf der man Umfragen <a href="#Freigeben">Freigeben </a><br> 
und <a href="#Fragen hinzufügen">Fragen hinzufügen</a> kann.</p>
<p> Hier wird auch schon der dazugehörige QR-Code und die ID angezeigt.</p>
<img src="images/faq/umfrage_bearbeiten.png" style="float:left ;width: 800px;"> 

<br>
<div style="clear: both; "/>
<h2><a name="Freigeben">4. Freigeben</a></h2>
<p>Hier werden zeilenweise die einzelnen GWDG-User aufgelistet, die eine Freigabe erhalten sollen.</p>
<img src="images/faq/freigabe.png" style="float: left; width: 900px;">
<div style="clear: both; "/>

<br>

<h2><a name="Fragenhinzufügen">5. Fragen hinzufügen</a></h2>
<img src="images/faq/antworten.png" style="float: left; width: 750px;"><p><br><br><br>Hier können nun Titel und Fragen der Umfrage eingestellt werden.
								<br><br>Mit der Option "Mehrfachteilnahme zulassen" können gleiche Personen mehrfach abstimmen. 
								<br><br>Als <a href="#Fragetypen">Fragetypen</a> gibt es "Single Choice", "Multiple Choice" und "Freitext" 									zur Auswahl.<br><br>
								Als <a href="#Darstellung">Darstellung </a>gibt es entweder ein Torten- oder Säulendiagramm. <br>
								<br><br>In dem Feld "Automatisch Beenden" kann in Sekungen angegeben werden, 
								wann der Test automatisch enden soll.<br>
								<br>Im letzten Feld "Antwortmöglichkeiten" werden die jeweiligen Antworten eingetragen, <br>
								wobei jede Zeile einer Antwortmöglichkeit entspricht.</p>
<div style="clear: both; "/>

<br>
 
<h2><a name="Fragetypen">6. Fragetypen</a></h2>

<h3>Single Choice</h3>
<p>Dabei kann stets nur eine Antwortmöglichkeit ausgwählt werden.</p>
<h3>Multiple Choice</h3>
<p>Dabei können beliebig viele Antwortmögleichkeiten ausgewählt werden.</p>

<h3>Freitext</h3>
<p>Hierbei werden keine Antwortmöglichkeiten vorgegeben und die Befragten können einen beliebigen Antworttext eingeben. Weiterhin besteht hier die Möglichkeit Wörter im Vorfeld zu verbieten und auf die <a href="#Blacklist">Blacklist</a> zu setzen.</p>

<br>
 
<h2><a name="Blacklist">7. Blacklist</a></h2>
<p>Unter dem Feld "Einstellungen" befindet sich die Möglichkeit Wörter für die Freitextantworten zu sperren.</p>
<p>Hier werden die zu sperrenden Wörter zeilenweise eingegeben.</p>
<img src="images/faq/blacklist.png" style="float: left; width: 800px;">
<div style="clear: both; "/>

<br>
 
<h2><a name="Darstellung">8. Darstellung</a></h2>
<p> Alle Farben werden zufällig gewählt. </p>
<h3>Tortendigramm</h3>
<img src="images/faq/torte.png" style="float: left; width: 400px;"> 
<div style="clear: both; "/>
<h3>Balkendiagramm</h3>
<img src="images/faq/balken.png" style="float: left; width: 400px;"> 
<div style="clear: both; "/>

<h3>Freitext (Wordcloud)</h3>
<img src="images/faq/freitext.png" style="float: left; width: 500px;">
<div style="clear: both; "/> 
<br>
 
<h2><a name="Präsentationsansicht">9. Präsentationsansicht</a></h2>
<p>Über das Säulen-Icon gelangen Sie in die Präsentationsansicht</p>
<img src="images/faq/eigene_umfrage.png" style="float: left; width: 1000px;"><br>
<div style="clear: both; "/>

<p>Sobald die Maus über den am linken Rand herausragenden "Kasten" zeigt, <br>
fährt dieser aus und es werden die dazugehörigen Durchläufe angezeigt.</p>
<img src="images/faq/praesentationsansicht.png" style="float: left; width: 700px;">
<div style="clear: both; "/>
<br>

<p>Weiterhin gibt es hier die Mögleichkeit einen <a href="#Neuen Durchlauf starten">neuen Durchlauf zu starten, <br>
</a><a href="#Durchläufe stoppen">einen Durchlauf zu stoppen </a> und <a href="#Durchläufe vergleichen"> Durchläufe zu vergleichen</a>.</p>
<img src="images/faq/klappenfenster.png" style="float: left; width: 400px;">	
<div style="clear: both; "/>

<br>

<h2><a name="NeuenDurchlaufstarten">10. Neuen Durchlauf starten</a></h2>
<p>Nach dem Start gelangen Sie zur Frage mit den Antwortmögleichkeiten. </p>
<img src="images/faq/neuer_durchlauf.png" style="float: left; width: 500px;">
<div style="clear: both; "/>
<br>
<p>Um nun den Studenten den QR-Code und Link zu zeigen, <br>muss nun einfach der <a href="#QR Code richtig anzeigen">QR-Code </a> ausgewählt werden.</p>
<img src="images/faq/qr1.png" style="float: left; width: 500px;">
<div style="clear: both; "/>

<br>

<h2><a name="QRCoderichtiganzeigen">11. QR Code richtig anzeigen</a></h2>
<p>(Hilfe:Was ist ein <a href="http://de.wikipedia.org/wiki/QR-Code"  target="_blank">QR-Code </a>?)</p>
<p>Es kann vorkommen, dass einige Handys den QR-Code aufgrund der Auflösung nicht einlesen können.<br>
Dann kann einfach über "+" und "-" die Größe des QR-Codes eingestellt werden. </p>
<img src="images/faq/qr2.png" style="float: left; width: 500px;"> 
<div style="clear: both; "/>

<br>

<h2><a name="Durchlaufstoppen">12. Durchlauf stoppen</a></h2>
<p>Einfach das "Pause-Icon" anklicken.</p>
<img src="images/faq/stop.png" style="float: left; width: 500px;">
<div style="clear: both; "/>

<br>

<h2><a name="Durchläufevergleichen">13. Durchläufe vergleichen</a></h2>
<p>Es besteht die Möglichkeit Durchläufe für die selbe Frage zu vergleichen. <br> Sie erhalten zum Vergleich ein Säulendiagramm.</p>
<img src="images/faq/vergleichen.png" style="float: left; width: 500px;">
<img src="images/faq/durchlauf_vergleichen.png" style="float: left; width: 500px;">
<div style="clear: both; "/>

<h2><a name="WeitereOptionen">14. Weitere Optionen</a></h2>
<p>Im <a href="#Hauptmenü">Hauptmenü</a> gibt es weitere Möglichkeiten die Umfragen zu bearbeiten.</p>

<img src="images/faq/optionen.png" style="float: left; width: 1000px;"> 
<br>
<div style="clear: both; "/>

<img src="images/faq/icon_praes.png" style="float: left;" class="small"> <p>Hiermit gelangen Sie zur Präsentationsansicht.</p>
<div style="clear: both; "/>
<img src="images/faq/icon_bearb.png" style="float: left;" class="small"><p>Hiermit gelangen Sie in den Bearbeitungsmodus.</p>
<div style="clear: both; "/>
<img src="images/faq/icon_zurue.png" style="float: left;" class="small"> <p>Hiermit kann eine Umfrage zurückgesetzt werden.</p>
<div style="clear: both; "/>
<img src="images/faq/icon_expo.png" style="float: left;" class="small"> <p>Hiermit können Umfragen als .CSV exportiert werden.</p>
<div style="clear: both; "/>
<img src="images/faq/icon_kopie.png" style="float: left;" class="small"> <p>Hiermit kann eine Umfrag kopiert bzw. dupliziert werden.</p>
<div style="clear: both; "/>
<img src="images/faq/icon_loesch.png" style="float: left;" class="small"> <p>Hiermit kann eine Umfrag komplett gelöscht werden.</p>
<div style="clear: both; "/>
<br>
	
	<h3 style="margin-top: 100px;">Credits</h3>
	<p>Es werden Icons von Mark James (<a href="http://famfamfam.com/lab/icons/silk/">http://famfamfam.com/lab/icons/silk/</a>, lizensiert unter der <a href="http://creativecommons.org/licenses/by/2.5/">Creative Commons Attribution 2.5 License</a>) verwendet.</p>
</div>
