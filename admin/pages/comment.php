<?php
$dbh = new PDO(PDO, DBUSERNAME, DBPASSWORD);
// parse id
$id = hexdec($_GET['id']);
if (!is_int($id)){
	exit();}
$s = $dbh->prepare("SELECT `status` FROM `mvote_umfrage` WHERE `mvote_umfrage`.`id` =".$id.";");
	$s->execute();
	$t=$s->fetch(PDO::FETCH_ASSOC);
	$success=($t['status']=='true')?1:0;
	if(!$success){?>
<div class="messageBoxRed" style="font-size: 1.5em; margin-top: 20px; text-align: center;">Kommentare deaktiviert</div><?php }		
		$q = $dbh->prepare("SELECT *
	FROM `mvote_umfrage` where `id`=".$id);
		$q->execute();
		$r = $q->fetchAll();
		$q = $dbh->prepare("SELECT *
	FROM `comments` where `frag_id`=$id ORDER BY id DESC");
		$q->execute();?>
	
	<div class="detailBox">
	<div style="clear:both; background:#FFF;"><div class="col-xs-8">
	  <h2>
		<?php  echo $r[0]['titel'];?>
	  </h2></div><div class="col-xs-2" style="text-align:right"><h3>Filter <a href="#" onclick="return star_all(<?php  echo $r[0]['id'];?>)"  title="Star Comment" id="star_click"><img src="./theme/icons/star_0.png" id="img_star"/></a></h3></div></div>  <button type="button" class="close" onclick="del_all('<?php echo $id?>')" style="float:right; margin:10px;">Alle Löschen</button>
	  <div class="clear"></div><div class="actionBox">
		<ul class="commentList" id="commentList">
		  <?php foreach($q->fetchAll() as $row) {
			?>
		  <li id="cmt_<?php echo $row['id']?>" class="<?php echo ($row['star'])?'':'s_0';?>">
			<div class="commentText">
			  <div  class="col-lg-10 row"><p><?php echo $row['comment']?></p>
			  <span class="date sub-text"><?php print date("d.m.Y H:i",$row['time']); ?> Uhr</span> </div><div class="commenterImage col-lg-1 row"><button type="button" class="close" onclick="del('<?php echo $row['id']?>')">&times;</button> </div>
			<div class="commenterImage col-lg-1 row" style="margin-right: 0px;"><a id="a_<?php echo $row['id']?>" href="<?php echo $row['star']?>" onclick="return star(<?php echo $row['id']?>,<?php echo $row['star']?>)"><img id="img_<?php echo $row['id']?>" src="theme/icons/star_<?php echo $row['star']?>.png"/></a></div>
			<div class="clear"></div></div>
		  </li>
		  <?php
		}?>
		</ul>
	  </div>
	</div>
	<input type="hidden" id="last" value=""/>
	<input type="hidden" id="last_r" value=""/>
	<div id="results"></div>
	<script>	$("#header").remove();
	r(<?php echo $id?>);
	var l=$("#last").val();
		setTimeout(function(){$( "#last_r").val(l);
			comment_r(<?php echo $id?>);
		},5000);
	function comment_r(id){
		var last_r = $( "#last_r" ).val();
		r(id);
		var last=$( "#last" ).val();
		if(last!==last_r){
			star_all_r(id);
		}
		setTimeout(function(){
			 comment_r(id);
		},5000); // Adjust the timeout value as you like
	}
	function r(id){
		$.ajax({
			 url: "ajax/comment_r.php?id="+id+"&last=1",
			cache: false
		})
		.done(function( html ) {
			$( "#last" ).val(html);
		});	
	}
	
	
	function star(id,star){
		$.ajax({
			 url: "ajax/comment.php?id="+id+"&star="+star+"&do=star",
			cache: false
		})
		.done(function( html ) {
			$( "#results" ).append( html );
		});
		return false;
	}
	function star_all(id){
		var t=$( "#star_click").attr( 'title');
		var u=s='';
		if(t=='Star Comment'){
			$( "#star_click").attr( 'title','All Comment');
			u="ajax/comment.php?id="+id+"&star=star&do=hs";
			s=1;
		}else if(t=='All Comment'){
			$( "#star_click").attr( 'title','Star Comment');
			u="ajax/comment.php?id="+id+"&star=all&do=hs";
			s=0;
		}
		$("#img_star").attr('src','./theme/icons/star_'+s+'.png');
		$.ajax({
				 url: u,
				cache: false
			})
			.done(function( html ) {
				$( "#commentList" ).html( html );
			});
			
		return false;
	}
	function star_all_r(id){
		var t=$( "#star_click").attr( 'title');
		if(t=='Star Comment'){
			u="ajax/comment.php?id="+id+"&star=all&do=hs";
		}else if(t=='All Comment'){
			$( "#star_click").attr( 'title','Star Comment');
			u="ajax/comment.php?id="+id+"&star=star&do=hs";
		}
		$.ajax({
				 url: u,
				cache: false
			})
			.done(function( html ) {
				$( "#commentList" ).html( html );
				
			$( "#last_r" ).val($( "#last" ).val());
			});
			
		return false;
	}
	function del(i) {
		var r = confirm('Wollen Sie diesen Kommentar wirklich löschen?');
		if (r == true) {		
			$.ajax({
				 url: "ajax/comment.php?id="+i+"&do=delete&all=0",
				cache: false
			})
			.done(function( html ) {
				$( "#results" ).append( html );
			});
			return false;
		}
		return false;
	}
	function del_all(i) {
		var r = confirm('Wollen Sie wirklich alle Kommentare löschen?');
		if (r == true) {		
			$.ajax({
				 url: "ajax/comment.php?id="+i+"&do=delete&all=1",
				cache: false
			})
			.done(function( html ) {
				$( "#results" ).html( html );
			});
			return false;
		}
		return false;
	}</script>
	<style type="text/css">
	.detailBox {
		width:500px;
		border:1px solid #bbb;
		margin:50px;clear: both;
	}
	.close{
		cursor:pointer;	
	}
	.commentBox {
		padding:10px;
		border-top:1px dotted #bbb;
	}
	.actionBox .form-group * {
		width:100%;
	}
	.taskDescription {
		margin-top:10px 0;
	}
	.commentList {
		padding:0;
		list-style:none;
	}
	.commentList li {
		margin:0;
		margin-top:10px;
		padding:3px;
		background:#DDD;
	}
	.commentList li > div {
		display:block;
	}
	.commenterImage {
		width:30px;
		height:100%;
		float:right;
	}
	.commenterImage img {
		width:100%;
		border-radius:50%;
	}
	.commentText{
		width:100%;	
	}
	.commentText p {
		margin:0;
	}
	.sub-text {
		color:#aaa;
		font-family:verdana;
		font-size:11px;
	}
	.actionBox {
		border-top:1px dotted #bbb;
		padding:10px;
	}
	</style>
