<?php

// Parse ID

$id = hexdec($_GET['id']);
if (!is_int($id))
	exit();

$dbh = new PDO(PDO, DBUSERNAME, DBPASSWORD);

if ($_GET['do'] == "edit") {
	$q = $dbh->prepare("UPDATE mvote_umfrage SET `titel` = :titel, `countdown` = :countdown, `freigabe` = :freigabe WHERE `id` = :id");
	$q->bindParam(":titel", $_POST['titel']);
	if ($_POST['countdown'] == "on")
		$c_state = 1;
	else
		$c_state = 0;
	$q->bindParam(":countdown", $c_state);
	$q->bindParam(":id", $id);
	$freigabe = strtolower($_POST['freigabe']);
	$q->bindParam(":freigabe", $freigabe);
    $q->execute();
	/*if ($q->execute()) {
		print "Erfolgreich";
	}
	else {
		print "Nicht erfolgreich!";
		$q->debugDumpParams();
		print_r($q->errorInfo());
	}*/
} elseif ($_GET['do'] == "newFrage") {


	$q_r = $dbh->prepare("SELECT reihenfolge FROM mvote_frage WHERE umfrage_id = :umfrage_id ORDER BY reihenfolge DESC LIMIT 1");
	$q_r->bindParam(":umfrage_id", $id);
	$q_r->execute();
	$r_r = $q_r->fetch(PDO::FETCH_ASSOC);
	$reihenfolge = $r_r['reihenfolge']+1;


	$q = $dbh->prepare("INSERT INTO `mvote_frage` (`id`, `titel`, `frage`, `erklaerung`, `zeit`, `reihenfolge`, `fragetyp`, `diagrammtyp`, `mehrfachteilnahme`, `enthaltung`, `antworten`, `r_antworten`, `umfrage_id`) VALUES (NULL, :titel, :frage, :erklaerung, :zeit, :reihenfolge, :fragetyp, :diagrammtyp, :mehrfachteilnahme, :enthaltung, :antworten, :r_antworten, :umfrage_id);");
	if ($_POST['mehrfachteilnahme'] == "on")
		$m_state = 1;
	else
		$m_state = 0;
    if ($_POST['enthaltung'] == "on")
		$enthaltung = 1;
	else
		$enthaltung = 0;
	if (empty($_POST['frage'])) {
		$f = "Es wurde keine Frage eingegeben!";
	} else {
		$f = $_POST['frage'];
	}
	$q->bindParam(":titel", $_POST['titel']);
	$q->bindParam(":frage", $f);
	$q->bindParam(":erklaerung", $_POST['erklaerung']);
	$q->bindParam(":zeit", $_POST['zeit']);
	$q->bindParam(":reihenfolge", $reihenfolge);
	$q->bindParam(":fragetyp", $_POST['fragetyp']);
	$q->bindParam(":diagrammtyp", $_POST['diagrammtyp']);
	$q->bindParam(":mehrfachteilnahme", $m_state);
    $q->bindParam(":enthaltung", $enthaltung);
	$q->bindParam(":antworten", trim($_POST['antworten']));
	$q->bindParam(":r_antworten", trim($_POST['r_antworten']));
	$q->bindParam(":umfrage_id", $id);
	/*if ($q->execute()) {
		print "Erfolgreich";
	}
	else {
		print "Nicht erfolgreich!";
		$q->debugDumpParams();
		print_r($q->errorInfo());
	}*/
    $q->execute();
	
} elseif ($_GET['do'] == "delFrage") {
		
	$q = $dbh->prepare("DELETE FROM mvote_frage WHERE `id` = :id");
	$q->bindParam(":id", $_GET['fid']);
	$q->execute();
} elseif ($_GET['do'] == "showEditFrage") {
	$q = $dbh->prepare("SELECT * FROM mvote_frage WHERE id = :id");
	$q->bindParam(":id", $_GET['fid']);
	$q->execute();
	$rf = $q->fetch(PDO::FETCH_ASSOC);
} elseif ($_GET['do'] == "editFrage") {
	$q = $dbh->prepare("UPDATE mvote_frage SET `titel` = :titel, `frage` = :frage, `erklaerung` = :erklaerung, `zeit` = :zeit, `fragetyp` = :fragetyp, `diagrammtyp` = :diagrammtyp, `mehrfachteilnahme` = :mehrfachteilnahme, `enthaltung` = :enthaltung, `antworten` = :antworten, `r_antworten` = :r_antworten WHERE `id` = :id");
	if (empty($_POST['frage'])) {
		$f = "Es wurde keine Frage eingegeben!";
	} else {
		$f = $_POST['frage'];
	}
	$q->bindParam(":id", $_GET['fid']);
	$q->bindParam(":titel", $_POST['titel']);
	$q->bindParam(":frage", $f);
	$q->bindParam(":erklaerung", $_POST['erklaerung']);
	$q->bindParam(":zeit", $_POST['zeit']);
	$q->bindParam(":fragetyp", $_POST['fragetyp']);
	$q->bindParam(":diagrammtyp", $_POST['diagrammtyp']);
	if ($_POST['mehrfachteilnahme'] == "on")
		$m_state = 1;
	else
		$m_state = 0;
	$q->bindParam(":mehrfachteilnahme", $m_state);
    if ($_POST['enthaltung'] == "on")
		$enthaltung = 1;
	else
		$enthaltung = 0;
    $q->bindParam(":enthaltung", $enthaltung);
	$q->bindParam(":antworten", trim($_POST['antworten']));
	$q->bindParam(":r_antworten", trim($_POST['r_antworten']));
	$q->execute();
	//var_dump($q->errorInfo());
} elseif ($_GET['do'] == "up") {
	
	// Reihenfolge normieren
	$q2 = $dbh->prepare("SELECT * FROM mvote_frage WHERE umfrage_id = :umfrage_id ORDER by reihenfolge ASC");
	$q2->bindParam(":umfrage_id", $id);
	$q2->execute();
	
	$r = 0;
	foreach($q2->fetchAll() as $row) {
		$q = $dbh->prepare("UPDATE mvote_frage SET `reihenfolge` = :reihenfolge WHERE `id` = :id");
		$q->bindParam(":id", $row['id']);
		$q->bindParam(":reihenfolge", $r);
		$q->execute();
		$r = $r + 2;
	}
	
	$qUp = $dbh->prepare("SELECT * FROM mvote_frage WHERE id = :fid");
	$qUp->bindParam(":fid", $_GET['fid']);
	$qUp->execute();
	$rUP = $qUp->fetch(PDO::FETCH_ASSOC);
	$reihenfolge = $rUP['reihenfolge'] - 3;
	
	$q = $dbh->prepare("UPDATE mvote_frage SET `reihenfolge` = :reihenfolge WHERE `id` = :id");
	$q->bindParam(":id", $_GET['fid']);
	$q->bindParam(":reihenfolge", $reihenfolge);
	$q->execute();
}  elseif ($_GET['do'] == "down") {
	
	// Reihenfolge normieren
	$q2 = $dbh->prepare("SELECT * FROM mvote_frage WHERE umfrage_id = :umfrage_id ORDER by reihenfolge ASC");
	$q2->bindParam(":umfrage_id", $id);
	$q2->execute();
	
	$r = 0;
	foreach($q2->fetchAll() as $row) {
		$q = $dbh->prepare("UPDATE mvote_frage SET `reihenfolge` = :reihenfolge WHERE `id` = :id");
		$q->bindParam(":id", $row['id']);
		$q->bindParam(":reihenfolge", $r);
		$q->execute();
		$r = $r + 2;
	}
	
	$qUp = $dbh->prepare("SELECT * FROM mvote_frage WHERE id = :fid");
	$qUp->bindParam(":fid", $_GET['fid']);
	$qUp->execute();
	$rUP = $qUp->fetch(PDO::FETCH_ASSOC);
	$reihenfolge = $rUP['reihenfolge'] + 3;
	$q = $dbh->prepare("UPDATE mvote_frage SET `reihenfolge` = :reihenfolge WHERE `id` = :id");
	$q->bindParam(":id", $_GET['fid']);
	$q->bindParam(":reihenfolge", $reihenfolge);
	$q->execute();
} elseif  ($_GET['do'] == "delDurchlauf") {
    $durchlauf_id = (int)$_GET['did'];
    
    $q = $dbh->prepare("DELETE FROM `mvote_durchlauf` WHERE `id` = :id");
	$q->bindParam(":id", $durchlauf_id);
	$q->execute();
}

$q = $dbh->prepare("SELECT * FROM mvote_umfrage WHERE id = :id");
$q->bindParam(":id", $id);
$q->execute();
$r = $q->fetch(PDO::FETCH_ASSOC);
?>
<div id="body">
	<div>
	<h1>Umfrage</h1>
	<div class="col-xs-9">
		<form id="edit-form" action="?p=editUmfrage&do=edit&id=<?php print strtoupper(dechex($r['id'])); ?>" method="post">
			<label class="col-xs-2">Titel:</label><input onkeydown="enable()" type="text" name="titel" value="<?php print $r['titel'];?>" class="col-sm-9">
            <div class="clear"></div>
			<label class="col-xs-2">Ersteller:</label><input type="text" value="<?php print $r['ersteller'];?>" disabled class="col-sm-9">
            <div class="clear"></div>
			<!--<p>10 Sekunden Countdown zum Ende? <input type="checkbox" name="countdown" style="width: 20px;" <?php if($r['countdown']) print "checked";?>></p>-->
		<h2 class="col-xs-11">Freigabe einrichten</h2>
		<p class="col-xs-11">Um diese Umfrage an Dritte freizugeben, geben Sie deren GWDG-Kennung (z. B. mmuster) ein (Eine Kennung pro Zeile).</p>
		<div  class="col-xs-11"><textarea onkeydown="enable()" name="freigabe" id="freigabe_email"><?php print $r['freigabe']; ?></textarea></div>
        <div class="clear"></div> 
			
		</form>
        <div id="emails"></div>
        <script>
        function freigabe(){
			var text=$('#freigabe_email').val();
			text=text.split('\n');
			var newArray = [];
			for (var i = 0; i < text.length; i++) {
	    		if (text[i] !== "" && text[i] !== null) {
	        		newArray.push(text[i]);
	    		}
			}	
			var e_l=newArray.length;
			var j=0;
			/*for(var i=0;i < newArray.length;i++){
				if(!isValidEmail(newArray[i])){
					alert("Invalid Emails");	
					return false;
				}
			}*/
			for(var i=0;i<e_l;i++){
				
				var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    			if(!regex.test(newArray[i])){
					newArray[i]=newArray[i]+'@gwdg.de';	
				}
				$.post("email-verification/emails_send.php", {
					email: newArray[i]
					,msg:'<?php print $r['titel'];?>'/*Message will be here*/,
					send_email:1
					},function(result){}).done(function() {
					j++;
				});
			}
		$("#edit-form").submit();
	}
	function isValidEmail(email) {
    	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    	return regex.test(email);
  	}
        </script>
	</div>
	<div class="col-xs-2">
		<img class="qrcode" src="qr.php?code=<?php print strtoupper(dechex($r['id'])); ?>">
		<h2 class="noborder">Umfrage-ID: #<?php print strtoupper(dechex($r['id'])); ?></h2>
	</div>
	<div  class="col-xs-11" />
	<button style="cursor:pointer" onclick='freigabe()' disabled="disabled" id="ck" class="col-sm-4">Speichern</button>
	<button style="cursor:pointer" onclick='c_d(<?php echo $r['id']; ?>)' id="c_d" class="col-sm-4 col-sm-offset-1"><?php echo ($r['status']=='true')?'Kommentare deaktivieren':'Kommentare aktivieren';?></button>
    <script>
	
		function enable(){
			$("#ck").prop('disabled', false);
		}
    function c_d(id){
		$("#c_d").prop('disabled', true);
		$.ajax({
			 url: "ajax/comment.php?id="+id+"&do=mark",
			cache: false
		})
		.done(function( html ) {
			var temp = $( "#c_d" ).html();
			if(temp=='Kommentare deaktivieren'){
				$( "#c_d" ).html('Kommentare aktivieren');
			}else{
				$( "#c_d" ).html('Kommentare deaktivieren');
			}$("#c_d").prop('disabled', false);
		});
		return false;
	}
    </script>
	</div>
    
    <div style="clear: both;" />
	<br />
	<h2>Fragen</h2>
	<table>
		<thead>
			<tr>
				<td>Frage</td>
				<td style="width: 50px;">Teilnehmer</td>
				<td style="width: 50px;">Status</td>
				<td style="width: 250px;">Optionen</td>
			</tr>
		</thead>
		<tbody>
		<?php 	
			$q2 = $dbh->prepare("SELECT * FROM mvote_frage WHERE umfrage_id = :umfrage_id ORDER by reihenfolge ASC");
			$q2->bindParam(":umfrage_id", $id);
			$q2->execute();
			
			foreach($q2->fetchAll() as $row) {
			?>
			<tr>
				<td><?php print $row['titel'];?></td>
				<td></td>
				<td></td>
				<td style="text-align: center;">
					<a href="?p=editUmfrage&id=<?php print strtoupper(dechex($r['id'])); ?>&do=showEditFrage&fid=<?php print $row['id'];?>"><img src="theme/icons/pencil32.png" title="Bearbeiten"></a>
					<a href="?p=editUmfrage&id=<?php print strtoupper(dechex($r['id'])); ?>&do=up&fid=<?php print $row['id'];?>"><img src="theme/icons/arrow_up.png" title="Nach oben verschieben"></a>
					<a href="?p=editUmfrage&id=<?php print strtoupper(dechex($r['id'])); ?>&do=down&fid=<?php print $row['id'];?>"><img src="theme/icons/arrow_down.png" title="Nach unten verschieben"></a>
					<a onclick='return confirm("Diese Frage wirklich löschen?");' href="?p=editUmfrage&id=<?php print strtoupper(dechex($r['id'])); ?>&do=delFrage&fid=<?php print $row['id'];?>"><img src="theme/icons/cross32.png" title="Löschen"></a>
					<?php if (false && $row['fragetyp'] != 2) { ?>
					<a href="?p=praesentation&id=<?php print strtoupper(dechex($r['id'])); ?>&fid=<?php print $row['id'];?>&do=compare" style="color: red;"><img src="theme/icons/chart_bar.png" title="Vergleich der Ergebnisse"><img src="theme/icons/chart_bar.png" title="Vergleich der Ergebnisse"></a></td>
					<?php } ?>
					<?php
					$q_durchlauf = $dbh->prepare("SELECT *, (SELECT COUNT(*) FROM mvote_teilnahme WHERE durchlauf_id = mvote_durchlauf.id) as count FROM mvote_durchlauf WHERE frage_id = :frage_id");
					$q_durchlauf->bindParam(":frage_id", $row['id']);
					$q_durchlauf->execute();
			
					foreach($q_durchlauf->fetchAll() as $row_d) {
					?>
					<tr class="durchlauf">
					<td class="durchlauf">Durchlauf: <?php print date("d.m.Y H:i",$row_d['start']); ?> Uhr</td><td><?php print $row_d['count']; ?></td>
                    <td style="text-align: center;"><?php if ($row_d['ende'] == 0 || $row_d['ende'] > time()) print '<img src="theme/icons/control_play_blue.png" title="Läuft">'; else print '<img src="theme/icons/stop32.png" title="Läuft nicht">'; ?></td><td style="text-align: center;"><a href="?p=praesentation&id=<?php print strtoupper(dechex($id)); ?>&did=<?php print $row_d['id']; ?>"><img src="theme/icons/chart_bar32.png" title="Präsentationsansicht"></a> <a onclick='return confirm("Diesen Durchlauf wirklich löschen?");' href="?p=editUmfrage&id=<?php print strtoupper(dechex($id)); ?>&did=<?php print $row_d['id']; ?>&do=delDurchlauf"><img src="theme/icons/cross32.png" title="Durchlauf löschen"></a></td>
					</tr>
					<?php
					}
					?>
			</tr>
			<?php
			}
			?>
		</tbody>
		
	</table>
	<p class="right"><button onclick='$("#create-frage").dialog("open");'><img src="theme/icons/add.png" height="15" width="15">Frage hinzufügen</button></p>
	
	<div id="create-frage" title="Neue Frage erstellen">
		<form action="?p=editUmfrage&id=<?php print strtoupper(dechex($r['id'])); ?>&do=newFrage" method="post">
			<div class="row">
                <label class="col-xs-2">Titel:</label> <input type="text" name="titel" value="" class="col-xs-9">
			</div>
            <div class="row">
                <label class="col-xs-2">Frage:</label> <input type="text" name="frage" value="" class="col-xs-9">
            </div>
			<!--<label>Erklärung:</label>--> <input type="hidden" name="erklaerung" value="">
            <div class="row">
                <label class="col-xs-2">Mehrfachteilnahme</label>
                <div class="col-xs-9">
                    <input type="checkbox" name="mehrfachteilnahme" style="width: 20px;"><br><br>
                </div>
			</div>
            <div class="row">
                <label class="col-xs-2">Fragetyp:</label> <select class="col-xs-9" id="fragetyp" name="fragetyp">
                    <option value="0">Single Choice</option>
                    <option value="1">Multiple Choice</option>
                    <option value="2">Freitext</option></select><br><br>
                <!--<span id="showEnthaltung" <?php if($rf['fragetyp'] != 1) { ?>style="display: none;"<?php } ?>><label>Enthaltung zulassen</label> <input type="checkbox" name="enthaltung" style="width: 20px;" <?php if($rf['enthaltung']) print "checked";?>></span>-->
			</div>
            <br><br>
            <div class="row">
                <label class="noFreitext col-xs-2">Diagrammtyp:</label>
                    <select class="col-xs-9 noFreitext" name="diagrammtyp">
                        <option value="0">Balkendiagramm</option>
                        <option value="1">Tortendiagramm</option>
                    </select>
            </div>
            <div class="row">
                <label class="col-xs-2">Automatisch beenden:</label>
                <input class="col-xs-9" type="text" name="zeit" value="">
			</div>
            <div class="row">
                <label class="noFreitext col-xs-2">Antwortmöglichkeiten (eine Antwort pro Zeile)</label>
                
                <textarea class="noFreitext col-xs-9" name="antworten"  id="ant" onchange="check()"></textarea>
            </div>
		</form>
	</div>
	<?php  if ($_GET['do'] == "showEditFrage") {?>
	<div id="edit-frage" title="Frage bearbeiten">
		<form action="?p=editUmfrage&id=<?php print strtoupper(dechex($r['id'])); ?>&do=editFrage&fid=<?php print $rf['id'];?>" method="post">
            <div class="row">
                <label class="col-xs-2">Titel:</label> <input type="text" name="titel" value="<?php print $rf['titel'];?>" class="col-xs-9">
			</div>
            <div class="row">
                <label class="col-xs-2">Frage:</label> <input type="text" name="frage" value="<?php print $rf['frage'];?>" class="col-xs-9">
            </div>
			<!--<label>Erklärung:</label>--> <input type="hidden" name="erklaerung" value="<?php print $rf['erklaerung'];?>">
            <div class="row">
                <label class="col-xs-2">Mehrfachteilnahme</label>
                <div class="col-xs-9">
                    <input type="checkbox" name="mehrfachteilnahme" style="width: 20px;" <?php if($rf['mehrfachteilnahme']) print "checked";?>><br><br>
                </div>
			</div>
            <div class="row">
                <label class="col-xs-2">Fragetyp:</label> <select class="col-xs-9" id="fragetyp" name="fragetyp"><option value="0"<?php if($rf['fragetyp'] == 0) print "selected";?>>Single Choice</option><option value="1" <?php if($rf['fragetyp'] == 1) print "selected";?>>Multiple Choice</option><option value="2" <?php if($rf['fragetyp'] == 2) print "selected";?>>Freitext</option></select><br><br>
                <span id="showEnthaltung" <?php if($rf['fragetyp'] != 1) { ?>style="display: none;"<?php } ?>><label>Enthaltung zulassen</label> <input type="checkbox" name="enthaltung" style="width: 20px;" <?php if($rf['enthaltung']) print "checked";?>></span>
			</div>
            <br><br>
            <div class="row">
                <label class="noFreitext col-xs-2">Diagrammtyp:</label>
                    <select class="col-xs-9 noFreitext" name="diagrammtyp"><option value="0" <?php if($rf['diagrammtyp'] == 0) print "selected";?>>Balkendiagramm</option><option value="1" <?php if($rf['diagrammtyp'] == 1) print "selected";?>>Tortendiagramm</option></select>
            </div>
            <div class="row">
                <label class="col-xs-2">Automatisch beenden:</label>
                <input class="col-xs-9" type="text" name="zeit" value="<?php print $rf['zeit'];?>">
			</div>
            <div class="row">
                <label class="noFreitext col-xs-2">Antwortmöglichkeiten (eine Antwort pro Zeile)</label>
                
                <textarea class="noFreitext col-xs-9" name="antworten"  id="ant" onchange="check()"><?php print $rf['antworten'];?></textarea>
            </div>
            <input type="hidden" id="r_antworten" name="r_antworten" value=""/>
            <input type="hidden" id="r_ans">
        </form>
	</div>
	<script>
	$(document).ready(function(){		
		function r_ans(){
			alert('Done');
			var text1=[];
			$('input[name="answer"]:checked').each(function(){
				text1.push(this.value);
			});
			text2=text1.toString();
			text2=text2.replace(/,/g,'\n');
			$("#r_antworten").val(text2);
		}
		
		$("#edit-frage").dialog("open");
			hideInputIfNeeded("#edit-frage");			
			
			/*Here click function is not working properly*/
			$('input[name="answer"]').click(function() {r_ans()});
		});
		function r(id){
    		$.ajax({
 				 url: "ajax/antworten_r.php?id="+id+"&last=1"
			})
  			.done(function( html ) {
				html=html.split('\n');
				$("#r_ans").val(html);
  			});	
		}
		function check(){
			var text=$('#ant').val();
			text=text.split('\n');
			var newArray = [];
			for (var i = 0; i < text.length; i++) {
	    		if (text[i] !== "" && text[i] !== null){
		        	newArray.push(text[i]);
		    	}
			}
			text=newArray;
			var l=text.length;
			$("#ans").html('');
			r(<?php echo $rf['id']?>);
			text1=$("#r_ans").val().split(',');
			var check='';
			for(i=0;i<l;i++){
				if($.inArray(text[i].trim(), text1) > -1)
    			{
        			check='checked';
    			}else{
					check='';
				}
				$("#ans").append('<div class="col-lg-12"><input class="answer" type="checkbox" name="answer" value="'+text[i]+'" '+check+' onclick(r_ans())>'+text[i]+'</div>');	
			}
		}
</script>
	<?php } ?>
</div>
