<?php
// Parse ID
$id = hexdec($_GET['id']);
$durchlauf_id = (int)$_GET['did'];
// parse id
if (!is_int($id))
	exit();
$question = loadQuestion($id, $durchlauf_id);
/*if ($_GET['do'] == "stop") {
	$question->stop($_GET['did']);
	$question = loadQuestion($id, $durchlauf_id);
} elseif ($_GET['do'] == "start") {
	
	if (!$question->isActive()) {
		$durchlauf_id = $question->start((int) $_GET['fid']);
		$question = loadQuestion($id, $durchlauf_id);
	} else {
		print '<script>alert("Es müssen erst alle Durchläufe gestoppt werden, bevor ein neuer Durchlauf gestartet werden kann!");</script>';
	}
}*/
if ($_GET['do'] != "compare") { ?>
<div id="body" class="praesentation col-lg-11 col-md-11 col-sm-11 row">
	<script>$("#prnt").css({ display: "block"});</script>
	<?php
	$frage = $question->getQuestion();
	if (empty($frage)) {
		print "<div class='messageBoxYellow'>Bitte wählen Sie eine Frage aus der Sidebar aus.</div>";
	} else { ?>
	<h1>Frage: <?php print $question->getQuestion(); ?></h1>
	<?php }
	if ($durchlauf_id == 0 || ($question->getEnde() == 0 || $question->getEnde() > time())) { ?>
	<img class="qrcode_max" src="qr.php?code=<?php print strtoupper(dechex($id)); ?>">
    <div id="resizeControll"><a href="javascript:resizeQRplus();"><img title="Zoom In" src="images/IN.png"></a><a href="javascript:resizeQRminus();"><img title="Zoom Out" src="images/OUT.png"></a>
	<h2><?= (SHORTROOTURL . $question->getHexId()) ?></h2></div>
	<?php }else{
		print $question->getPresentation();
	}?>	
</div>
<?php } ?>
<?php if ($_GET['do'] == "compare") { ?>
<div id="body" class="praesentation col-lg-11 col-md-11 col-sm-11 row">	

	<?php
	// Frage: Antworten holen
	$q = $dbh->prepare("SELECT frage, antworten FROM `mvote_frage` WHERE id = :frage_id AND umfrage_id = :umfrage_id");
	$q->bindParam(":frage_id", $_GET['fid']);
	$q->bindParam(":umfrage_id", $id);
	$q->execute();
	$r = $q->fetch(PDO::FETCH_ASSOC);
	$antworten = explode("<br />", nl2br($r['antworten'])); ?>
	<script>$("#prnt").css({ display: "block"});</script>
    <h1>Vergleich: <?php print $r['frage']; ?></h1>
	<?php
	// Alle Durchläufe
	
	$q_durchlauf = $dbh->prepare("SELECT * FROM mvote_durchlauf WHERE frage_id = :frage_id");
	$q_durchlauf->bindParam(":frage_id", $_GET['fid']);
	$q_durchlauf->execute();
	
	$total=0;
	foreach($q_durchlauf->fetchAll() as $row_d){
		foreach ($antworten as $v=>$a) {
			$qa = $dbh->prepare("SELECT COUNT(*) as count FROM `mvote_durchlauf`, `mvote_teilnahme` WHERE mvote_durchlauf.id = :durchlauf_id AND mvote_durchlauf.id = mvote_teilnahme.durchlauf_id AND mvote_teilnahme.antwort = :antwort");
			$qa->bindParam(":durchlauf_id", $row_d['id']);
			$qa->bindParam(":antwort", $v);
			$qa->execute();
			$ra = $qa->fetch(PDO::FETCH_ASSOC);
			$total= $total+$ra['count'];	
		}
	}
	$data = '{labels:["'.trim($antworten[0]).'"';
	for ($i=1; count($antworten) > $i; $i++)
		$data .= ',"'.trim($antworten[$i]).'"';
	$data .= '], datasets : [';
	$color = 0;
	$desc = '<div>';
	
	$q_durchlauf = $dbh->prepare("SELECT * FROM mvote_durchlauf WHERE frage_id = :frage_id");
	$q_durchlauf->bindParam(":frage_id", $_GET['fid']);
	$q_durchlauf->execute();
	$countDurchlaeufe = 0;
	$max = 0;
	$total=0;
	$j=1;
	$desc .='<div class="color_pres col-lg-2">';
	foreach($q_durchlauf->fetchAll() as $row_d){
		if ($countDurchlaeufe != 0) {
			$data.=",";
		}
		$countDurchlaeufe++;
		$data.='{fillColor: "'.getColor($color).'", strokeColor: "'.getColor($color).'", data: [';
		foreach ($antworten as $v=>$a) {
			if ($antworten[0] != $a) {
				$data .= ",";
			}
		
			$qa = $dbh->prepare("SELECT COUNT(*) as count FROM `mvote_durchlauf`, `mvote_teilnahme` WHERE mvote_durchlauf.id = :durchlauf_id AND mvote_durchlauf.id = mvote_teilnahme.durchlauf_id AND mvote_teilnahme.antwort = :antwort");
			$qa->bindParam(":durchlauf_id", $row_d['id']);
			$qa->bindParam(":antwort", $v);
			$qa->execute();
			$ra = $qa->fetch(PDO::FETCH_ASSOC);
			$data .= $ra['count'];
			$total=$total+$ra['count'];
			if ($ra['count'] > $max)
				$max = $ra['count'];
		}$data .= ']}';
		$desc.="<div style='margin:5px; float:left;  text-align:left;'><div style='padding:10px; background:".getColor($color)."; font-size: 1em; float:left'></div><strong> ".$j."</strong> Durchlauf</div>";
		$temp=0;
		$j++;
		$color++;
		$countDurchlaeufe++;
	}	
	$data .= ']}';
	$desc .= "</div></div>";
	print $desc;
	?>
	<canvas id="diagramm" width="500" height="350"></canvas>
    <script>
	var data = <?php print $data; ?>;
	var ctx = $("#diagramm").get(0).getContext("2d");
	var myNewChart = new Chart(ctx).Bar(data, {scaleOverride : true,scaleStepWidth : <?php $stepWidth = round($max/10); if ($stepWidth == 0) $stepWidth = 1;print $stepWidth; ?>,scaleSteps : <?php print ceil($max/$stepWidth);?>, scaleStartValue : 0});
	</script>
</div>
<?php }?>
<div id="sidebar">
</div>

<div id="sidebar1" class="col-lg-1"></div>
<script>
$(document).ready(function() {
	ladeSidebar('<?php print strtoupper(dechex($id)); ?>');
	ladeSidebar1('<?php print strtoupper(dechex($id)); ?>');
});
</script>
