<?php
$_SESSION['isLoggedIn'] = false;
?>
<div id="body" class="container">
  <?php
	// source: http://stackoverflow.com/a/5302437
	if(preg_match('/(?i)msie [4-8]/',$_SERVER['HTTP_USER_AGENT'])) {
	print "<div style='font-size:1em;' class='messageBoxRed'>Sie verwenden eine veraltete Version des Internet Explorers. Leider kann die Funktionsfähigkeit von mVote nicht mit Ihrem Browser gewährleistet werden.<br>
		<br>
		Bitte verwenden Sie alternativ <strong>Mozilla Firefox</strong> oder <strong>Google Chrome</strong>.</div>";
	} ?>
  <?php if (isset($errorMsg)) {
		print '<p class="alert">'.$errorMsg.'</p>';
	}
	?>
    
  <div class="col-xs-12"><div class="col-xs-6 col-xs-offset-3" style="display: none;"><h5 style="float:right"><a href="#" id="l_s" onkeyup="iframeLoaded()">Registrieren</a></h5>
    <script>
	function iframeLoaded(){
      var iFrameID = document.getElementById('idIframe');
      if(iFrameID) {
            // here you can make the height, I delete it first, then I make it again
            iFrameID.height = "";
            iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
      }   
  }
	$( "#l_s" ).click(function() {
		var l = $("#l_s").html();
		if(l=="Registrieren"){
			$("#login_c").css({
				display: 'none'
			});
			$("#sign_c").css({
				display: 'block'
			});
			iframeLoaded();
			$("#l_s").html("Login");
		}
		if(l=="Login"){
			$("#login_c").css({
				display: 'block'
			});
			$("#sign_c").css({
				display: 'none'
			});
			$("#l_s").html("Registrieren");
		}
		return false;
	});
    </script>
    </div>


<!--<div style='font-size:1em;' class='messageBoxRed'>Bitte beachten Sie, dass am Dienstag, den 23.08.2016, ab 12:00 Uhr Wartungsarbeiten stattfinden werden. mVote wird daher ab 12:00 Uhr zeitweise nicht verfügbar sein.</div>-->

    <div   id="login_c" class="col-md-6 col-md-offset-3" style="margin-top:15px; box-sizing:border-box; display:block;">
      <h2 class="col-lg-12">Login</h2><h6>Bitte melden Sie sich mit Ihrer GWDG-Kennung an (z. B. mmuster)</h6>
      <form action="<?php print ROOTURL ?>admin/" method="POST">
        <label for="user" class="col-lg-4" style="text-align:right"><strong>GWDG-Kennung</strong></label>
        <div class="col-lg-6">
          <input type="text" class="col-lg-12" id="user" name="user" placeholder="GWDG-Kennung" required>
        </div>
        <label for="passwort" class="col-lg-4" style="text-align:right"><strong>GWDG-Passwort</strong></label>
        <div class="col-lg-6">
          <input type="password" class="col-lg-12" id="passwort" name="passwort" placeholder="GWDG-Passwort" required>
        </div>
        <div class="col-lg-11" style="text-align:right">
          <button style="margin:20px auto">Login</button>
        </div>
      </form>
      <div class="clear"></div>
    </div>
    <div class="col-md-5 col-md-offset-3" style="margin-top:15px; box-sizing:border-box; display:none;" id="sign_c"><h2 class="col-lg-12">Registrieren</h2>
    <iframe src="email-verification/index.php" scrolling="auto"  class="col-xs-12" style="margin-top:15px; box-sizing:border-box; border:none;" id="idIframe"></iframe>
    </div>
    <div class="clear"></div>
    <div class="col-lg-12 row links"><h2>Einsatzszenarien</h2><a href="http://www.uni-goettingen.de/de/Einsatzszenarien/500623.html"  class="col-lg-2 col-md-3 col-sm-6 row col-xs-6" target="_blank"><img src="images/1_Vorstellung.png" /></a>
    <a href="http://www.uni-goettingen.de/de/Einsatzszenarien/500628.html"  class="col-lg-2 col-md-3 col-sm-6 row col-xs-6" target="_blank"><img src="images/2_Recall-Frage_blau.png" /></a>
    <a href="http://www.uni-goettingen.de/de/Einsatzszenarien/500630.html"  class="col-lg-2 col-md-3 col-sm-6 row col-xs-6" target="_blank"><img src="images/3_Quiz.png" /></a>
    <a href="http://www.uni-goettingen.de/de/Einsatzszenarien/500632.html"  class="col-lg-2 col-md-3 col-sm-6 row col-xs-6" target="_blank"><img src="images/4_Orga.png" /></a>
    <a href="http://www.uni-goettingen.de/de/Einsatzszenarien/500633.html"  class="col-lg-2 col-md-3 col-sm-6 row col-xs-6" target="_blank"><img src="images/5_Evaluation.png" /></a>
    <a href="http://www.uni-goettingen.de/de/Einsatzszenarien/500634.html"  class="col-lg-2 col-md-3 col-sm-6 row col-xs-6" target="_blank"><img src="images/6_Peer Discussion.png" /></a>
    <a href="http://www.uni-goettingen.de/de/Einsatzszenarien/500636.html"  class="col-lg-2 col-md-3 col-sm-6 row col-xs-6" target="_blank"><img src="images/7_Umgestaltung Vorlesung.png" /></a>
    <a href="http://www.uni-goettingen.de/de/Einsatzszenarien/500635.html"  class="col-lg-2 col-md-3 col-sm-6 row col-xs-6" target="_blank"><img src="images/8_Vergleich.png" /></a>
    <a href="http://www.uni-goettingen.de/de/Einsatzszenarien/500637.html"  class="col-lg-2 col-md-3 col-sm-6 row col-xs-6" target="_blank"><img src="images/9_Hörsaalspiele.png" /></a>
    <a href="http://www.uni-goettingen.de/de/Einsatzszenarien/500638.html"  class="col-lg-2 col-md-3 col-sm-6 row col-xs-6" target="_blank"><img src="images/10_Inverted Classroom.png" /></a> </div>
  </div>
</div>
