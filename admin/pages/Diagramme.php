<div id="body">
	<h1>Seite nicht gefunden</h1>
	<canvas id="bla1" width="350" height="350"></canvas><br>
	<canvas id="bla2" width="350" height="350"></canvas>
	<script>
		var data = {
			'values'	:	[['Null', 10],['Eins', 123],['Zwei', 75],['Drei', 200],['Vier', 100]] //,['Drei', 1000],['Drei', 200],['Drei', 200]]
		};
		zeichneBalkendiagramm("bla1", data);
		zeichneKreisdiagramm("bla2", data);
	</script>
</div>
