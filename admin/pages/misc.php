<?php

// stupid 'access control'
$whitelist = array('user1', 'user2', 'user3', 'user4');
if (!in_array($_SESSION['userID'], $whitelist)) {
    include('pages/Error404.php');
    exit;
}

$dbh = new PDO(PDO, DBUSERNAME, DBPASSWORD);

// #umfrage
$q = $dbh->prepare("SELECT count(*) as count FROM `mvote_umfrage`");
$q->execute();
$r = $q->fetch(PDO::FETCH_ASSOC);
$umfragen = $r['count'];

// #fragen
$q = $dbh->prepare("SELECT count(*) as count FROM `mvote_frage`");
$q->execute();
$r = $q->fetch(PDO::FETCH_ASSOC);
$fragen = $r['count'];

// #fragetype Single Choice
$q = $dbh->prepare("SELECT count(*) as count FROM `mvote_frage` WHERE fragetyp = 0");
$q->execute();
$r = $q->fetch(PDO::FETCH_ASSOC);
$fragetyp0 = $r['count'];

// #fragetype Multiple Choice
$q = $dbh->prepare("SELECT count(*) as count FROM `mvote_frage` WHERE fragetyp = 1");
$q->execute();
$r = $q->fetch(PDO::FETCH_ASSOC);
$fragetyp1 = $r['count'];

// #fragetyp Freitext
$q = $dbh->prepare("SELECT count(*) as count FROM `mvote_frage` WHERE fragetyp = 2");
$q->execute();
$r = $q->fetch(PDO::FETCH_ASSOC);
$fragetyp2 = $r['count'];

// #mehrfachteilnahme
$q = $dbh->prepare("SELECT count(*) as count FROM `mvote_frage` WHERE mehrfachteilnahme = 1");
$q->execute();
$r = $q->fetch(PDO::FETCH_ASSOC);
$mehrfachteilnahme = $r['count'];

// #durchlaeufe
$q = $dbh->prepare("SELECT count(*) as count FROM `mvote_durchlauf`");
$q->execute();
$r = $q->fetch(PDO::FETCH_ASSOC);
$durchlaeufe = $r['count'];

// #teilnehmer
$q = $dbh->prepare("SELECT count(*) as count FROM `mvote_teilnahme`");
$q->execute();
$r = $q->fetch(PDO::FETCH_ASSOC);
$teilnehmer = $r['count'];

// #dozenten
$q = $dbh->prepare("SELECT count(*) as count FROM (SELECT * FROM `mvote_log_events` WHERE isLoggedIn = 1 GROUP by user) as users");
$q->execute();
$r = $q->fetch(PDO::FETCH_ASSOC);
$dozenten = $r['count'];

// load teilnehmer per month
$q = $dbh->prepare("SELECT month, year, SUM(db.count) as sum FROM (SELECT MONTH(FROM_UNIXTIME(start)) as month, YEAR(FROM_UNIXTIME(start)) as year, (SELECT count(*) FROM mvote_teilnahme WHERE mvote_teilnahme.durchlauf_id = mvote_durchlauf.id) as count FROM `mvote_durchlauf`) as db GROUP BY month, year ORDER BY year, month ASC");
$q->execute();
$timeline = $q->fetchAll(PDO::FETCH_ASSOC);


// load TOP 15 user -> umfragen
$q = $dbh->prepare("SELECT ersteller, count( * ) AS anzahl_umfrage
FROM `mvote_umfrage`
GROUP BY ersteller
ORDER by anzahl_umfrage DESC
LIMIT 0 , 15
");
$q->execute();
$top15umfragen = $q->fetchAll(PDO::FETCH_ASSOC);

// load TOP 15 user -> fragen
$q = $dbh->prepare("SELECT ersteller, count( * ) AS anzahl_fragen
FROM `mvote_umfrage` , mvote_frage
WHERE mvote_umfrage.id = mvote_frage.umfrage_id
GROUP BY ersteller
ORDER by anzahl_fragen DESC
LIMIT 0 , 15");
$q->execute();
$top15fragen = $q->fetchAll(PDO::FETCH_ASSOC);

// load TOP 15 user -> durchläufe
$q = $dbh->prepare("SELECT ersteller, count( * ) AS anzahl_durchlaeufe
FROM `mvote_umfrage` , mvote_frage, mvote_durchlauf
WHERE mvote_umfrage.id = mvote_frage.umfrage_id
AND mvote_frage.id = mvote_durchlauf.frage_id
GROUP BY ersteller
ORDER by anzahl_durchlaeufe DESC
LIMIT 0 , 15");
$q->execute();
$top15durchlauefe = $q->fetchAll(PDO::FETCH_ASSOC);

// load TOP 15 user -> teilnahmen
$q = $dbh->prepare("SELECT ersteller, count( * ) AS anzahl_teilnahmen
FROM `mvote_umfrage` , mvote_frage, mvote_durchlauf, `mvote_teilnahme`
WHERE mvote_umfrage.id = mvote_frage.umfrage_id
AND mvote_frage.id = mvote_durchlauf.frage_id
AND mvote_durchlauf.id = `mvote_teilnahme`.durchlauf_id
GROUP BY ersteller
ORDER by anzahl_teilnahmen DESC
LIMIT 0 , 15");
$q->execute();
$top15teilnahmen = $q->fetchAll(PDO::FETCH_ASSOC);


$data = '{labels:[';
foreach ($timeline as $t) {
    $data .= '"' . $t['year'].'-'.$t['month'].'",';
}
$data .= '], datasets : [';
    
$data .= '{fillColor: "#84cbe5", strokeColor: "#0099CC", data: [';
foreach ($timeline as $t) {
    $data .= $t['sum'].',';
}
$data .= ']}';
$data .= ']}';

// load teilnehmer per year
$q = $dbh->prepare("SELECT year, SUM(db.count) as sum FROM (SELECT YEAR(FROM_UNIXTIME(start)) as year, (SELECT count(*) FROM mvote_teilnahme WHERE mvote_teilnahme.durchlauf_id = mvote_durchlauf.id) as count FROM `mvote_durchlauf`) as db GROUP BY year ORDER BY year ASC");
$q->execute();
$timeline = $q->fetchAll(PDO::FETCH_ASSOC);

$data2 = '{labels:[';
foreach ($timeline as $t) {
    $data2 .= '"' . $t['year'] . '",';
}
$data2 .= '], datasets : [';
    
$data2 .= '{fillColor: "#84cbe5", strokeColor: "#0099CC", data: [';
foreach ($timeline as $t) {
    $data2 .= $t['sum'].',';
}
$data2 .= ']}';
$data2 .= ']}';
?>
<div id="body">
	<h1>Statistik</h1>
	<ul>
        <li>Anzahl Umfragen: <?= $umfragen; ?></li>
        <li>Anzahl Fragen: <?= $fragen; ?> <small>(Mehrfachteilnahme aktiviert: <?= $mehrfachteilnahme; ?> | Single Choice: <?= $fragetyp0; ?>, Multiple Choice: <?= $fragetyp1; ?>, Freitext: <?= $fragetyp2; ?>)</small></li>
        <li>Anzahl Durchläufe: <?= $durchlaeufe; ?></li>
        <li>Anzahl Lehrende: <?= $dozenten; ?></li>
        <li>Anzahl Studierende: <?= $teilnehmer; ?></li>
    </ul>
    
    <h2>#Studierende pro Monat <small style="color: gray;">entspricht der Anzahl Abstimmungen pro Monat</small></h2>
    <canvas id="diagramm" width="1200" height="500"></canvas>
	<script>
	var data = <?php print $data; ?>;

	var ctx = $("#diagramm").get(0).getContext("2d");
	var myNewChart = new Chart(ctx).Line(data);
	</script>
    
    <h2>#Studierende pro Jahr <small style="color: gray;">entspricht der Anzahl Abstimmungen pro Jahr</small></h2>
    <canvas id="diagramm2" width="1200" height="500"></canvas>
	<script>
	var data2 = <?php print $data2; ?>;

	var ctx = $("#diagramm2").get(0).getContext("2d");
	var myNewChart = new Chart(ctx).Line(data2);
	</script>
    
    <h2>TOP 15 Umfragen</h2>
    <table style="width: 500px;">
        <thead>
            <th style="width: 250px;">User</th><th style="width: 250px;">Anzahl Umfragen</th>
        </thead>
        <tbody>
            <? foreach ($top15umfragen as $e): ?>
            <tr>
                <td><?= $e['ersteller']; ?></td>
                <td><?= $e['anzahl_umfrage']; ?></td>
            </tr>
            <? endforeach; ?>
        </tbody>
    </table>
    
    <h2>TOP 15 Fragen</h2>
    <table style="width: 500px;">
        <thead>
            <th style="width: 250px;">User</th><th style="width: 250px;">Anzahl Fragen</th>
        </thead>
        <tbody>
            <? foreach ($top15fragen as $e): ?>
            <tr>
                <td><?= $e['ersteller']; ?></td>
                <td><?= $e['anzahl_fragen']; ?></td>
            </tr>
            <? endforeach; ?>
        </tbody>
    </table>
    
    <h2>TOP 15 Durchläufe</h2>
    <table style="width: 500px;">
        <thead>
            <th style="width: 250px;">User</th><th style="width: 250px;">Anzahl Durchläufe</th>
        </thead>
        <tbody>
            <? foreach ($top15durchlauefe as $e): ?>
            <tr>
                <td><?= $e['ersteller']; ?></td>
                <td><?= $e['anzahl_durchlaeufe']; ?></td>
            </tr>
            <? endforeach; ?>
        </tbody>
    </table>
    
    <h2>TOP 15 Abstimmungen</h2>
    <table style="width: 500px;">
        <thead>
            <th style="width: 250px;">User</th><th style="width: 250px;">Anzahl Abstimmungen</th>
        </thead>
        <tbody>
            <? foreach ($top15teilnahmen as $e): ?>
            <tr>
                <td><?= $e['ersteller']; ?></td>
                <td><?= $e['anzahl_teilnahmen']; ?></td>
            </tr>
            <? endforeach; ?>
        </tbody>
    </table>
</div>
