<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<?php
if (isset($_POST["sub"])) {
  $name = trim($_POST["f_name"]);
  $pass = trim($_POST["pass1"]);
  $email = trim($_POST["uemail"]);
  $sql = "SELECT COUNT(*) AS count from tbl_users where email = :email_id";
  try {
    $stmt = $DB->prepare($sql);
    $stmt->bindValue(":email_id", $email);
    $stmt->execute();
    $result = $stmt->fetchAll();
    if ($result[0]["count"] > 0) {
      $msg = "Email already exist";
      $msgType = "warning";
    } else {
      $sql = "INSERT INTO `tbl_users` (`name`, `pass`, `email`) VALUES " . "( :name, :pass, :email)";
      $stmt = $DB->prepare($sql);
      $stmt->bindValue(":name", $name);
      $stmt->bindValue(":pass", md5($pass));
      $stmt->bindValue(":email", $email);
      $stmt->execute();
      $result = $stmt->rowCount();
 
      if ($result > 0) {
 
        $lastID = $DB->lastInsertId();
        $msg = "User registered successfully";
        $msgType = "success";
 
      } else {
        $msg = "Failed to create User";
        $msgType = "warning";
      }
    }
  } catch (Exception $ex) {
    echo $ex->getMessage();
  }
}
?><form class="form-horizontal contactform" action="index.php" method="post" name="f">
  <fieldset>
  <div class="form-group">
    <label>Name:
      <input type="text" placeholder="Your Name" id="f_name" class="form-control" name="f_name">
      </label>
    </div>
 
    <div class="form-group">
      <label class="col-lg-12 control-label" for="uemail">Email:
        <input type="text" placeholder="Your Email" id="uemail" class="form-control" name="uemail">
        </label>
      </div>
 
      <div class="form-group">
        <label class="col-lg-12 control-label" for="pass1">Password:
        <input type="password" placeholder="Password" id="pass1" class="form-control" name="pass1">
        </label>
     </div>
 
     <div class="form-group">
       <label class="col-lg-12 control-label" for="pass1">Confirm Password:
       <input type="password" placeholder="Password" id="pass2" class="form-control" name="pass2">
        </label>
       </div>
 
       <div class="form-group">
       <div class="col-lg-10">
       <button class="btn btn-primary" type="submit" name="sub">Submit</button> 
       </div>
       </div>
   </fieldset>
</form>
</body>

</html>