<?php

$dbh = new PDO(PDO, DBUSERNAME, DBPASSWORD);
// insert
if ($_GET['do'] == "new") {
	print "NEW";
	$q = $dbh->prepare("INSERT INTO `mvote_blackliste` (`ersteller`, `woerter`) VALUES (:ersteller, :woerter);");
	$q->bindParam(":woerter", $_POST['woerter']);
	$q->bindParam(":ersteller", $_SESSION['userID']);
	$q->execute();
} else if ($_GET['do'] == 'edit') {
	$q = $dbh->prepare("UPDATE mvote_blackliste SET `woerter` = :woerter WHERE `ersteller` = :ersteller");
	$q->bindParam(":woerter", $_POST['woerter']);
	$q->bindParam(":ersteller", $_SESSION['userID']);
	$q->execute();
}
	

// lade Blackliste
$q = $dbh->prepare("SELECT woerter, count(*) as count FROM mvote_blackliste WHERE ersteller = :ersteller LIMIT 1");
$q->bindParam(":ersteller", $_SESSION['userID']);
$q->execute();
$r = $q->fetch(PDO::FETCH_ASSOC);

if ($r['count'] == 0) {
	$typ = 'new';
} else {
	$typ = 'edit'; 
}

?>
<div id="body">
	<h1>Einstellungen</h1>
	
	<h2>Blacklist für Freitext-Umfragen</h2>
	<p>Bitte geben Sie Wörter, die in Freitextfragen geblockt werden sollen, in das Textfeld ein (ein Wort pro Zeile):</p>
	<form action="?p=settings&do=<?php print $typ; ?>" method="post">
		<textarea name="woerter"><?php print $r['woerter']; ?></textarea><br>
		<button>Speichern</button>
	</form>
</div>
