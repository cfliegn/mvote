<?php
error_reporting(E_ERROR);
foreach ($_GET as $a=>$b) {
	$_GET[$a] = htmlspecialchars($b);
}
foreach ($_POST as $a=>$b) {
	$_POST[$a] = htmlspecialchars($b);
}
$page = htmlspecialchars($_GET['p']);
include("lib/config.php");
include("lib/auth.php");
if (empty($page)) {
	$page = "index";
}
if (substr_count($page, '.') != 0 OR substr_count($page, '/') != 0 OR !file_exists("pages/".$page.".php")) {
	header("HTTP/1.1 404 Not Found");
	$page = "Error404";
}
include("theme/header.php");
include("pages/".$page.".php");
include("theme/footer.php");
?>