<div data-role="page" id="start" data-theme="a">
	<div data-role="header">
		<h1>mVote</h1>
	</div><!-- /header -->
	<div data-role="content">	
		<p>Bitte geben Sie den Umfragecode ein, um an der Umfrage teilzunehmen:</p>	
		<form action="?p=umfrage" id="codeForm">
			<div data-role="fieldcontain">
				<label for="code">Umfragecode:</label>
				<input name="code" id="code" type="text" placeholder="Code eingeben" required>
			</div>
			<center><button data-inline="true">Teilnehmen</button></center>
		</form>	
	</div><!-- /content -->
</div>
