<?php
$dbh = new PDO(PDO, DBUSERNAME, DBPASSWORD);
$id = hexdec($_GET['code']);

$q = $dbh->prepare("SELECT mvote_durchlauf.id as id, frage, antworten FROM `mvote_frage`, `mvote_durchlauf` WHERE mvote_frage.umfrage_id = :id AND mvote_frage.id = mvote_durchlauf.frage_id AND (mvote_durchlauf.ende = 0 OR mvote_durchlauf.ende > :ende)");
$q->bindParam(":id", $id);
$time = time();
$q->bindParam(":ende", $time);
$q->execute();
$r = $q->fetch(PDO::FETCH_ASSOC);

$antworten = split("<br />", nl2br($r['antworten']));
if ($_GET['toLate'] == 1) {
	$m = '<div class="redbg" style="padding: 10px;"><h1>Ihre Stimme wurde <strong>nicht</strong> gezählt!</h1>Die Zeit ist schon abgelaufen!</div>';
} else if ($_GET['again'] == 1) {
	var_dump($_GET);
	$m = '<div class="redbg" style="padding: 10px;"><h1>Ihre Stimme wurde <strong>nicht</strong> gezählt!</h1>Es sind keine Mehrfachteilnahmen erlaubt!</div>';
}else {
	$m = '<div class="greenbg" style="padding: 10px;"><h1>Ihre Stimme wurde gezählt!</h1>Vielen Dank!</div>';
}
if (empty($r['frage']) || empty($r['antworten'])) {
?>
<div data-role="page" id="warten" data-theme="a" data-add-back-btn="false">

	<div data-role="header">
		<h1>Umfrage #<?php print strtoupper(dechex($id)); ?></h1>
	</div><!-- /header -->

	<div data-role="content">
		<?php print $m; 
		$s = $dbh->prepare("SELECT `status` FROM `mvote_umfrage` WHERE `mvote_umfrage`.`id` =".$id.";");
	$s->execute();
	$t=$s->fetch(PDO::FETCH_ASSOC);
	$success=($t['status']=='true')?1:0;
	if($success){
		?><div id="comment">
<h4>Frage an den Dozierenden senden</h4>
	<form action="index.php?p=umfrage&code=<?php print strtoupper(dechex($id)); ?>" method="post">
    	<textarea name="comment" id="comment" placeholder="Frage eingeben"></textarea>
        <input type="submit" name="com_sub" value="Absenden"/>
        <input type="hidden" name="code" value="<?php echo $_GET['code']?>" />
        <input type="hidden" name="p" value="umfrage" />
    </form></div>
	<?php }
		?>
		
		<!--<p>Aktuell können keine weiteren Fragen der gewählten Umfrage beantwortet werden.</p>-->
		<!--<center><a href="#" onclick="location.reload(); return false;" data-ajax="false" data-icon="refresh" data-role="button" data-inline="true">Nach neuer Frage suchen</a></center>-->
		<center><a href="index.php?p=umfrage&code=<?php print dechex($id); ?>" data-ajax="false" data-icon="refresh" data-role="button" data-inline="false">Nach neuer Frage suchen</a></center><br />
<br />
<br />
	</div><!-- /content -->
</div><!-- /page -->
<?php
} else {
?>
<div data-role="page" id="umfrage" data-add-back-btn="false" data-back-btn-text="Zurück" data-theme="a">

	<div data-role="header">
		<h1>Umfrage #<?php print strtoupper(dechex($id)); ?></h1>
	</div><!-- /header -->

	<div data-role="content">
		<?php print $m; $s = $dbh->prepare("SELECT `status` FROM `mvote_umfrage` WHERE `mvote_umfrage`.`id` =".$id.";");
	$s->execute();
	$t=$s->fetch(PDO::FETCH_ASSOC);
	$success=($t['status']=='true')?1:0;
	if($success){
		?><div id="comment">
<h4>Frage an den Dozierenden senden</h4>
	<form action="index.php?p=umfrage&code=<?php print strtoupper(dechex($id)); ?>" method="post">
    	<textarea name="comment" id="comment" placeholder="Frage eingeben"></textarea>
        <input type="submit" name="com_sub" value="Absenden"/>
        <input type="hidden" name="code" value="<?php echo $_GET['code']?>" />
        <input type="hidden" name="p" value="umfrage" />
    </form></div>
	<?php }?>
		<!--<p>Es kann folgende Frage beantwortet werden:</p>
		<ul data-role="listview" data-inset="true">
		<li><a href="?p=umfrage&code=ID<?php print strtoupper(dechex($id)); ?>"><?php print $r['frage']; ?></a></li>
		</ul>
		<!--<center><a href="?p=umfrage&code=ID<?php print strtoupper(dechex($id)); ?>" data-ajax="false" data-role="button" data-inline="true">Teilnehmen</a></center>-->
		<!--<br>
		<br>-->
		<!--<center><a href="#" onclick="location.reload(); return false;" data-ajax="false" data-icon="refresh" data-role="button">Nach neuer Frage suchen</a></center>-->
		<center><a href="index.php?p=umfrage&code=<?php print dechex($id); ?>" data-ajax="false" data-icon="refresh" data-role="button" data-inline="false">Nach neuer Frage suchen</a></center>
	</div><!-- /content -->

</div><!-- /page -->
<?php
}
?>
