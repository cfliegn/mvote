<?php
// get decimal id
$id = hexdec($_GET['code']);
// load question
$dbh = new PDO(PDO, DBUSERNAME, DBPASSWORD);
$q = $dbh->prepare("SELECT mvote_durchlauf.id as id, frage, antworten, enthaltung, fragetyp FROM `mvote_frage`, `mvote_durchlauf` WHERE mvote_frage.umfrage_id = :id AND mvote_frage.id = mvote_durchlauf.frage_id AND (mvote_durchlauf.ende = 0 OR mvote_durchlauf.ende > :ende)");
$q->bindParam(":id", $id);
$time = time();
$q->bindParam(":ende", $time);
$q->execute();
$r = $q->fetch(PDO::FETCH_ASSOC);

// create question class
include("questions/QTemplate.php");
if ($r['fragetyp'] == 0) {
	include("questions/SingleChoice.php");
	$question = new SingleChoice();
} elseif ($r['fragetyp'] == 1) {
	include("questions/MultipleChoice.php");
	$question = new MultipleChoice();
} elseif ($r['fragetyp'] == 2) {
	include("questions/FreeText.php");
	$question = new FreeText();
}
$question->loadQuestion($id, $r);
if (!$question->isActive()) {
?>
<div data-role="page" id="warten" data-add-back-btn="false" data-theme="a">
	<div data-role="header">
		<h1>Umfrage #<?php print $question->getHexId(); ?></h1>
	</div><!-- /header -->

	<div data-role="content">	
		<p>Aktuell können keine Fragen der gewählten Umfrage beantwortet werden.</p>
		<center><a href="#" onclick="location.reload(); return false;" data-ajax="false" data-icon="refresh" data-role="button" data-inline="true">Nach neuer Frage suchen</a></center>

<?php
} else {
?>
<div data-role="page" id="umfrage" data-add-back-btn="false" data-back-btn-text="Zurück" data-theme="a">
	<div data-role="header">
		<h1>Umfrage #<?php print $question->getHexId(); ?></h1>
	</div><!-- /header -->
	<div data-role="content">
		<?php
$success=0;
if(isset($_POST['comment']) && isset($_POST['code']) && $_POST['code']!='' && trim($_POST['comment'])!=''){
	$q = $dbh->prepare("INSERT INTO `comments` (`id`, `frag_id`, `comment`, `star`, `time`) VALUES (NULL, '".$id."', :c, '0',:time);");
	$time = time();
	$q->bindParam(":time", $time);
	$comment = trim($_POST['comment']);
	$q->bindParam(":c", $comment);
	$q->execute();
}			
		print "<h2>".$question->getQuestion()."</h2>";
		print $question->getAnswersHTML();
		?>
		<br>
		<br>
		 
		 <center><a href="?p=umfrage&code=<?php print $question->getHexId(); ?>" data-ajax="false" data-icon="refresh" data-role="button" data-inline="false">Nach neuer Frage suchen</a></center>
	<br />
<br />
<br />


<?php
}
?>

<?php 
	$s = $dbh->prepare("SELECT `status` FROM `mvote_umfrage` WHERE `mvote_umfrage`.`id` =".$id.";");
	$s->execute();
	$t=$s->fetch(PDO::FETCH_ASSOC);
	$success=($t['status']=='true')?1:0;
	if($success){?><div id="comment">
<h4>Frage an den Dozierenden senden</h4>
	<form action="?p=umfrage&code=<?php print $question->getHexId(); ?>" method="post">
    	<textarea name="comment" id="comment" placeholder="Frage eingeben"></textarea>
        <input type="submit" name="com_sub" value="Absenden"/>
        <input type="hidden" name="code" value="<?php echo $_GET['code']?>" />
        <input type="hidden" name="p" value="umfrage" />
        <input type="hidden" name="id" value="" />
    </form></div><?php }?></div><!-- /content -->
</div><!-- /page -->

