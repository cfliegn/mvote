<?php
include("lib/config.php");
$id = hexdec($_GET['code']);
$dbh = new PDO(PDO, DBUSERNAME, DBPASSWORD);

// überprüfen ob Frage identisch ist bei neuem durchlauf
function frageEquals($durchlauf_neu, $durchlauf_alt) {
	global $dbh;
	$q1 = $dbh->prepare("SELECT frage_id FROM  `mvote_durchlauf` WHERE  `id` = :d");
	$q1->bindParam(":d", $durchlauf_neu);
	$q1->execute();
	$r1 = $q1->fetch(PDO::FETCH_ASSOC);
	$q2 = $dbh->prepare("SELECT frage_id FROM  `mvote_durchlauf` WHERE  `id` = :d");
	$q2->bindParam(":d", $durchlauf_alt);
	$q2->execute();
	$r2 = $q2->fetch(PDO::FETCH_ASSOC);
	
	if ($r1['frage_id'] == $r2['frage_id']) {
		return true;
	}
	
	return false;
}

// Überprüfen ob Durchlauf aktiv ist:
$q = $dbh->prepare("SELECT mvote_durchlauf.id as id, frage, antworten, fragetyp, mehrfachteilnahme FROM `mvote_frage`, `mvote_durchlauf` WHERE mvote_frage.umfrage_id = :id AND mvote_frage.id = mvote_durchlauf.frage_id AND (mvote_durchlauf.ende = 0 OR mvote_durchlauf.ende > :ende)");

$q->bindParam(":id", $id);
$time = time();
$q->bindParam(":ende", $time);
$q->execute();
$r = $q->fetch(PDO::FETCH_ASSOC);

// create question class
include("questions/QTemplate.php");
if ($r['fragetyp'] == 0) {
	include("questions/SingleChoice.php");
	$question = new SingleChoice();
} elseif ($r['fragetyp'] == 1) {
	include("questions/MultipleChoice.php");
	$question = new MultipleChoice();
} elseif ($r['fragetyp'] == 2) {
	include("questions/FreeText.php");
	$question = new FreeText();
}

$question->loadQuestion($id, $r);

// Überprüfe ob Druchlauf aktiv ist
if ($question->isActive() && ($question->getDurchlaufId() == $_GET['d'] || frageEquals($question->getDurchlaufId(), $_GET['d']))) {
	// generate hash
	$hash = md5($r['id'] . $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'] . date("dmY"));
	
	// falls mehrfachteilname nicht erlaubt ist, überprüfe ob bereits abgestimmt wurde
	if ($r['mehrfachteilnahme'] == 0) {
		$q = $dbh->prepare("SELECT count(*) as count FROM `mvote_teilnahme` WHERE hash = :hash");
		$q->bindParam(":hash", $hash);
		$q->execute();
		$r = $q->fetch(PDO::FETCH_ASSOC);
		
		// nicht zählen falls mehrfachteilname
		if ($r['count'] != 0) {
			header('Location: index.php?p=abgestimmt&again=1&code=' . strtoupper(dechex($id)));
			exit();
		}
	}
	$_GET['d'] = $question->getDurchlaufId();
	$question->saveAnswer($_GET, $_POST, $hash);
	header('Location: index.php?p=abgestimmt&code=' . strtoupper(dechex($id)));
	exit();
	
}

header('Location: index.php?p=abgestimmt&toLate=1&code=' . strtoupper(dechex($id)));
?>
