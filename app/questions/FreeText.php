<?php
class FreeText extends QTemplate {
	
	private $data;
	private $id;
	private $dbh;
	
	function __construct() {
		$this->dbh = new PDO(PDO, DBUSERNAME, DBPASSWORD);
	}
	
	function getTitle() {
		return $this->data['title'];
	}
	
	function getQuestion() {
		return $this->data['frage'];
	}
	
	function getAnswersHTML() {
		$string = '<form action="vote.php?code='.$this->getHexId().'&d='.$this->data['id'].'" method="get"><div  data-role="fieldcontain" style="width: 100%;"><fieldset data-role="controlgroup" style="width: 100%;">
		<label for="answer">Antwort:</label>
	        <input type="text" name="answer" id="answer" value=""  style="width: 100%;" /><br>
		<button>Abschicken</button>
		</fieldset></div></form>';
		
		return $string;
	}
	
	function getAnswersHTMLForPresentation() {
		$string = '<img class="qrcode_max" src="qr.php?code=' . $this->getHexId() . '"> <div id="resizeControll"><a href="javascript:resizeQRplus();"><img title="Zoom In" src="images/IN.png"></a><a href="javascript:resizeQRminus();"><img title="Zoom In" src="images/OUT.png"></a></div>
	<h2>' . (SHORTROOTURL . $this->getHexId()) . '</h2>';
    
		return $string;
	}
	
	function isActive() {
		$q = $this->dbh->prepare("SELECT COUNT(*) as count FROM `mvote_durchlauf`, `mvote_frage` WHERE `mvote_frage`.umfrage_id = :id AND `mvote_frage`.id = `mvote_durchlauf`.frage_id AND (mvote_durchlauf.ende = 0 OR mvote_durchlauf.ende > :ende)");
		$q->bindParam(":id", $this->id);
		$time = time();
		$q->bindParam(":ende", $time);
		$q->execute();
		$r = $q->fetch(PDO::FETCH_ASSOC);
		if ($r['count'] > 0)
			return true;
		else
			return false;
	}
	
	function getHexId() {
		return strtoupper(dechex($this->id));
	}
	
	function getDurchlaufId() {
		return $this->data['id'];
	}
	
	function loadQuestion($decimalId, $dataArray) {
		$this->id = $decimalId;
		$this->data = $dataArray;
	}
	
	function getEnde() {
		return $this->data['ende'];
	}
	
	function stop($durchlauf_id) {
		$q = $this->dbh->prepare("UPDATE mvote_durchlauf SET `ende` = :ende WHERE `id` = :id");
		$q->bindParam(":id", $durchlauf_id);
		$time = time() - 1;
		$q->bindParam(":ende", $time);
		$q->execute();
		
	}
	
	function start($frage_id, $ende = 0) {
		if ($this->isActive()) {
			return false;
		}
		
		$q = $this->dbh->prepare("INSERT INTO `mvote_durchlauf` (`start`, `frage_id`, `ende`) VALUES (:start, :frage_id, :ende);");
		$q->bindParam(":frage_id", $frage_id);
		$time = time() - 1;
		$q->bindParam(":start", $time);
		$q->bindParam(":ende", $ende);
		$q->execute();
		
		$q = $this->dbh->prepare("SELECT id FROM `mvote_durchlauf` WHERE `frage_id` = :frage_id AND (mvote_durchlauf.ende = 0 OR mvote_durchlauf.ende > :ende)");
		$q->bindParam(":frage_id", $frage_id);
		$time = time();
		$q->bindParam(":ende", $time);
		$q->execute();
		$r = $q->fetch(PDO::FETCH_ASSOC);
		
		$durchlauf_id = $r['id'];
		
		return $durchlauf_id;
	}
	
	function saveAnswer($get, $post, $hash) {
		
		$q = $this->dbh->prepare("INSERT INTO `mvote_teilnahme` (`id`, `antwort`, `hash`, `durchlauf_id`) VALUES (NULL, :antwort, :hash, :durchlauf_id);");
		$q->bindParam(":antwort", $get['answer']);
		$q->bindParam(":durchlauf_id", $get['d']);
		$q->bindParam(":hash", $hash);
		print($q->execute());
	}
	
	function getPresentation() {
	
		// Lade Blackliste
		$q = $this->dbh->prepare("SELECT woerter FROM mvote_blackliste WHERE ersteller = :ersteller LIMIT 1");
		$q->bindParam(":ersteller", $_SESSION['userID']);
		$q->execute();
		$r = $q->fetch(PDO::FETCH_ASSOC);
		
		$blackliste = explode("<br />", trim(nl2br(strtolower($r['woerter']))));
		foreach ($blackliste as $k=>$v) {
			$blackliste[$k]= trim($v);
		}
		
		$q = $this->dbh->prepare("SELECT antwort, count(*) as count FROM `mvote_teilnahme` WHERE durchlauf_id = :durchlauf_id GROUP by antwort");
		$q->bindParam(":durchlauf_id", $this->data['id']);
		$q->execute();
		
		$min = PHP_INT_MAX;
		$max = 0;
		$words = array();
		foreach($q->fetchAll() as $row) {
			if (array_search(strtolower(trim($row['antwort'])), $blackliste) === false) {
				$words[$row['antwort']] = $row['count'];
			}
		}
		
		arsort($words);
		$output = "<div class='messageBoxGray'><ul>";
		if (count($words) > 0) {
			foreach($words as $a=>$b) {
				$output .= "<li>".htmlspecialchars ($a)." <small>(".$b.")</small></li>";
			}
		} else {
			$output .= "<li>Es wurde kein Text eingegeben</li>";
		}
		$output .= "</div></ul>";
		return $output;
		
	}
	
	function getFontSize($value, $max, $min) {
		$max_font_size = 50;
		$min_font_size = 15;
		
		return ($max_font_size - $min_font_size) * $value/$max + $min_font_size;
	}
		
}
?>
