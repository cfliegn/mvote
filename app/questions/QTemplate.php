<?php
abstract class QTemplate {
	private $data;
	private $id;
	private $dbh;
	function __construct() {
		$this->dbh = new PDO(PDO, DBUSERNAME, DBPASSWORD);
	}
	abstract function getTitle();
	abstract function getQuestion();
	abstract function getAnswersHTML();
	abstract function isActive();
	abstract function getHexId();
	abstract function getDurchlaufId();
	abstract function loadQuestion($decimalId, $dataArray);
	abstract function saveAnswer($get, $post, $hash);
	abstract function getPresentation();	
}
?>
