<?php
class SingleChoice extends QTemplate {
	private $data;
	private $id;
	private $dbh;
	
	function __construct() {
		$this->dbh = new PDO(PDO, DBUSERNAME, DBPASSWORD);
	}
	function get_total(){
		return $this->data['count'];	
	}
	function getTitle() {
		return $this->data['title'];
	}
	
	function getQuestion() {
		return $this->data['frage'];
	}
	
	function getAnswersHTML() {
		$string = '<ul data-role="listview" data-inset="true">';
		$antworten = explode("<br />", nl2br($this->data['antworten']));
		foreach ($antworten as $v=>$a) {
			$string .= '<li><a data-ajax="false" href="vote.php?code='.$this->getHexId().'&d='.$this->data['id'].'&v='.$v.'">'.$a.'</a></li>';
		}
		
		$string .= '</ul>';
        if ($this->data['enthaltung']) {
            $string .= '<br><br><a href="vote.php?code='.$this->getHexId().'&d='.$this->data['id'].'&e=1" data-role="button">Keine Antwort wählen</a>';
        }
		return $string;
	}
	
	function getAnswersHTMLForPresentation() {
		$string = '<ul class="showAnswersForPresentation">';
		$antworten = explode("<br />", nl2br($this->data['antworten']));
		foreach ($antworten as $v=>$a) {
			$string .= '<li>'.$a.'</li>';
		}
		$string .= '</ul>';
		
		$string .= '<input type="hidden" id="did" value="'.$this->getDurchlaufId().'"><div id="resizeControll"></div>';
		
		
		return $string;
	}
	
	function isActive() {
		$q = $this->dbh->prepare("SELECT COUNT(*) as count FROM `mvote_durchlauf`, `mvote_frage` WHERE `mvote_frage`.umfrage_id = :id AND `mvote_frage`.id = `mvote_durchlauf`.frage_id AND (mvote_durchlauf.ende = 0 OR mvote_durchlauf.ende > :ende)");
		$q->bindParam(":id", $this->id);
		$time = time();
		$q->bindParam(":ende", $time);
		$q->execute();
		$r = $q->fetch(PDO::FETCH_ASSOC);
		if ($r['count'] > 0)
			return true;
		else
			return false;
	}
	
	function getHexId() {
		return strtoupper(dechex($this->id));
	}
	
	function getDurchlaufId() {
		return $this->data['id'];
	}
	
	function loadQuestion($decimalId, $dataArray) {
		$this->id = $decimalId;
		$this->data = $dataArray;
	}
	
	function getEnde() {
		return $this->data['ende'];
	}
	
	function stop($durchlauf_id) {
		$q = $this->dbh->prepare("UPDATE mvote_durchlauf SET `ende` = :ende WHERE `id` = :id");
		$q->bindParam(":id", $durchlauf_id);
		$time = time() - 1;
		$q->bindParam(":ende", $time);
		$q->execute();
		
	}
	
	function start($frage_id, $ende = 0) {
		if ($this->isActive()) {
			return false;
		}
		
		$q = $this->dbh->prepare("INSERT INTO `mvote_durchlauf` (`start`, `frage_id`, `ende`) VALUES (:start, :frage_id, :ende);");
		$q->bindParam(":frage_id", $frage_id);
		$time = time() - 1;
		$q->bindParam(":start", $time);
		$q->bindParam(":ende", $ende);
		$q->execute();
		
		$q = $this->dbh->prepare("SELECT id FROM `mvote_durchlauf` WHERE `frage_id` = :frage_id AND (mvote_durchlauf.ende = 0 OR mvote_durchlauf.ende > :ende)");
		$q->bindParam(":frage_id", $frage_id);
		$time = time();
		$q->bindParam(":ende", $time);
		$q->execute();
		$r = $q->fetch(PDO::FETCH_ASSOC);
		
		$durchlauf_id = $r['id'];
		
		return $durchlauf_id;
	}
	
	function saveAnswer($get, $post, $hash) {
		if($get['e'] == 1) {
            $v = 'DER_NUTZER_HAT_SICH_ENTHALTEN';
            $q = $this->dbh->prepare("INSERT INTO `mvote_teilnahme` (`id`, `antwort`, `hash`, `durchlauf_id`) VALUES (NULL, :antwort, :hash, :durchlauf_id);");
            $q->bindParam(":antwort", $v);
            $q->bindParam(":durchlauf_id", $get['d']);
            $q->bindParam(":hash", $hash);
            $q->execute();
            
        }else{
            $q = $this->dbh->prepare("INSERT INTO `mvote_teilnahme` (`id`, `antwort`, `hash`, `durchlauf_id`) VALUES (NULL, :antwort, :hash, :durchlauf_id);");
            $q->bindParam(":antwort", $get['v']);
            $q->bindParam(":durchlauf_id", $get['d']);
            $q->bindParam(":hash", $hash);
            $q->execute();
        }
	}

	function getPresentation() {
		$total=0;
		$desc='';
		if ($this->data['diagrammtyp'] == 0) {
			$color = 0;
		
			$values = "{labels:[";
			$antworten = explode("<br />", nl2br($this->data['antworten']));
			
			$desc.= '<div style="width: 310px; left: 50px; position: absolute; top: 110px;">';
			
			$values .= '""], datasets : [';
			$c = 0;
			$max = 0;
			foreach ($antworten as $v=>$a) {
				$qa = $this->dbh->prepare("SELECT COUNT(*) as count FROM `mvote_durchlauf`, `mvote_teilnahme` WHERE mvote_durchlauf.id = :durchlauf_id AND mvote_durchlauf.id = mvote_teilnahme.durchlauf_id AND mvote_teilnahme.antwort = :antwort");
				$qa->bindParam(":durchlauf_id", $this->data['id']);
				$qa->bindParam(":antwort", $v);
				$qa->execute();
				$ra = $qa->fetch(PDO::FETCH_ASSOC);
				$total=$total+$ra['count'];
			}
			
			foreach ($antworten as $v=>$a) {
				$qa = $this->dbh->prepare("SELECT COUNT(*) as count FROM `mvote_durchlauf`, `mvote_teilnahme` WHERE mvote_durchlauf.id = :durchlauf_id AND mvote_durchlauf.id = mvote_teilnahme.durchlauf_id AND mvote_teilnahme.antwort = :antwort");
				$qa->bindParam(":durchlauf_id", $this->data['id']);
				$qa->bindParam(":antwort", $v);
				$qa->execute();
				$ra = $qa->fetch(PDO::FETCH_ASSOC);
				if ($c != 0)
					$values .= ",";
				$values .= "{ data: [".$ra['count']."], fillColor: '".getColor($color)."', strokeColor: '".getColor($color)."'}";
				$c++;
				$temp=sprintf("%.2f", ($ra['count']*$total)/100);
				$desc.="<div style='margin: 10px; text-align:left; font-size: 1.5em;'><div style='padding:15px; background:".getColor($color)."; float:left;'></div>".$antworten[$v]."[".round($ra['count']*100/$this->data['count'])."%]</div>" ;
				$color++;
				if ($ra['count'] > $max)
					$max = $ra['count'];
			}
			$values .= "]};";
			$desc .= '<input type="hidden" id="r_antworten" name="r_antworten" value=""/>
            <input type="hidden" id="r_ans"></div>';
		//$max = $this->data['count'];
		
		$output = $desc . '<div class="col-log-8"><canvas id="diagramm" width="700" height="500"></canvas>
		<h2>'.$total.' Teilnehmer</h2></div>
	
	<script>
	var data = '.$values.'
	var ctx = $("#diagramm").get(0).getContext("2d");
	var myNewChart = new Chart(ctx).Bar(data, {scaleOverride : true,scaleStepWidth : ';
	$stepWidth = round($max/10);
	if ($stepWidth == 0)
		$stepWidth = 1;
	$output .= $stepWidth.',scaleSteps : '.ceil($max/$stepWidth).', scaleStartValue : 0});';
	/*
	Here this function is not working correctly
	$output .= 'var myVar = setInterval(function(){ ladeErgebnisse("'.strtoupper($_GET['id']).'",'.$this->data['id'].') }, 5000);
	setInterval(function(){ ladeErgebnisse("'.strtoupper($_GET['id']).'",'.$this->data['id'].')}, 5000);
	myStopFunction();
	function myStopFunction() {
	    clearInterval(myVar);
	}';*/
	$output .='</script>';
	return $output;
		}
		// Tortendiagramm
		if ($this->data['diagrammtyp'] == 1) {
			$color = 0;
		
			$values = "[";
			$antworten = explode("<br />", nl2br($this->data['antworten']));
			$c = 0;
			$desc = '<div class="color_pres col-lg-2">';
			foreach ($antworten as $v=>$a) {
				$qa = $this->dbh->prepare("SELECT COUNT(*) as count FROM `mvote_durchlauf`, `mvote_teilnahme` WHERE mvote_durchlauf.id = :durchlauf_id AND mvote_durchlauf.id = mvote_teilnahme.durchlauf_id AND mvote_teilnahme.antwort = :antwort");
				$qa->bindParam(":durchlauf_id", $this->data['id']);
				$qa->bindParam(":antwort", $v);
				$qa->execute();
				$ra = $qa->fetch(PDO::FETCH_ASSOC);
				if ($c != 0)
					$values .= ",";
				$values .= "{ value: ".$ra['count'].", color: '".getColor($color)."'}";
				$c++;
				$desc.="<div style='margin: 10px; text-align:left; font-size: 1.5em;'><div style='padding:15px; background:".getColor($color)."; float:left;'></div>".$antworten[$v]."[".round($ra['count']*100/$this->data['count'])."%]</div>" ;
				$color++;
			}
			$values .= "]";
			$desc .= '
            <input type="hidden" id="r_antworten" name="r_antworten" value=""/>
            <input type="hidden" id="r_ans"></div>';
			$output = $desc.'<canvas id="diagramm" width="500" height="500"></canvas><h2>'.$this->data['count'].' Teilnehmer</h2>
			<script>
			var data = '.$values.'
	
			var ctx = $("#diagramm").get(0).getContext("2d");
			var myNewChart = new Chart(ctx).Pie(data);
			';
	/*
	Here this function is not working correctly
	
	$output .= 'var myVar = setInterval(function(){ ladeErgebnisse("'.strtoupper($_GET['id']).'",'.$this->data['id'].') }, 5000);
	setInterval(function(){ ladeErgebnisse("'.strtoupper($_GET['id']).'",'.$this->data['id'].')}, 5000);
	myStopFunction();
	function myStopFunction() {
	    clearInterval(myVar);
	}';*/
	$output .='</script>';return $output;
		}
	}
}
?>
