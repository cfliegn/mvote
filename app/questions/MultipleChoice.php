<?php
class MultipleChoice extends QTemplate {
	private $data;
	private $id;
	private $dbh;
	
	function __construct() {
		$this->dbh = new PDO(PDO, DBUSERNAME, DBPASSWORD);
	}
	
	function getTitle() {
		return $this->data['title'];
	}
	function getQuestion() {
		return $this->data['frage'];
	}
	
	function getAnswersHTML() {
		$string = '<form id="ans_sub" action="vote.php?code='.$this->getHexId().'&d='.$this->data['id'].'" method="get"><div  data-role="fieldcontain"  style="width: 100%;"><fieldset data-role="controlgroup"  style="width: 100%;">';
		$antworten = explode("<br />", nl2br($this->data['antworten']));
        $array = array(" ", ".");
		foreach ($antworten as $v=>$a) {
			$string .= '<input type="checkbox" name="check-'.md5(trim(str_replace($array, "_", $a))).'" id="check-'.$v.'" class="custom"  style="width: 100%;" /><label for="check-'.$v.'">'.$a.'</label>';
		}
		$string .= '<button>Abstimmen</button><br><br>';
        
		$string .= '</fieldset>';
        if ($this->data['enthaltung']) {
            $string .= '<a href="vote.php?code='.$this->getHexId().'&d='.$this->data['id'].'&e=1" data-role="button">Keine der genannten Antworten</a>';
        }
        $string .= '</div></form>';
        $string .= "<script>
                        $(document).ready(function() {
                        $('#ans_sub').on('submit', function() {
 if(!$('input[type=checkbox]:checked').length) {
                                alert('Bitte wählen Sie mindestens eine Antwort aus. " . ($this->data['enthaltung'] ? "oder wählen Sie >>Keine der genannten Varianten<< aus." : '') . "');
                                return false;
                            }
                            
                        });
                    });
                    </script>";
		return $string;
	}
	
	function getAnswersHTMLForPresentation() {
		$string = '<ul class="showAnswersForPresentation">';
		$antworten = explode("<br />", nl2br($this->data['antworten']));
		foreach ($antworten as $v=>$a) {
			$string .= '<li>'.$a.'</li>';
		}
		$string .= '</ul>';
		
		$string .= '<input type="hidden" id="did" value="'.$this->getDurchlaufId().'">';
		return $string;
	}
	
	function isActive() {
		$q = $this->dbh->prepare("SELECT COUNT(*) as count FROM `mvote_durchlauf`, `mvote_frage` WHERE `mvote_frage`.umfrage_id = :id AND `mvote_frage`.id = `mvote_durchlauf`.frage_id AND (mvote_durchlauf.ende = 0 OR mvote_durchlauf.ende > :ende)");
		$q->bindParam(":id", $this->id);
		$time = time();
		$q->bindParam(":ende", $time);
		$q->execute();
		$r = $q->fetch(PDO::FETCH_ASSOC);
		if ($r['count'] > 0)
			return true;
		else
			return false;
	}
	
	function getHexId() {
		return strtoupper(dechex($this->id));
	}
	
	function getDurchlaufId() {
		return $this->data['id'];
	}
	
	function loadQuestion($decimalId, $dataArray) {
		$this->id = $decimalId;
		$this->data = $dataArray;
	}
	
	function getEnde() {
		return $this->data['ende'];
	}
	
	function stop($durchlauf_id) {
		$q = $this->dbh->prepare("UPDATE mvote_durchlauf SET `ende` = :ende WHERE `id` = :id");
		$q->bindParam(":id", $durchlauf_id);
		$time = time() - 1;
		$q->bindParam(":ende", $time);
		$q->execute();
	}
	
	function start($frage_id, $ende = 0) {
		if ($this->isActive()) {
			return false;
		}
		
		$q = $this->dbh->prepare("INSERT INTO `mvote_durchlauf` (`start`, `frage_id`, `ende`) VALUES (:start, :frage_id, :ende);");
		$q->bindParam(":frage_id", $frage_id);
		$time = time() - 1;
		$q->bindParam(":start", $time);
		$q->bindParam(":ende", $ende);
		$q->execute();
		
		$q = $this->dbh->prepare("SELECT id FROM `mvote_durchlauf` WHERE `frage_id` = :frage_id AND (mvote_durchlauf.ende = 0 OR mvote_durchlauf.ende > :ende)");
		$q->bindParam(":frage_id", $frage_id);
		$time = time();
		$q->bindParam(":ende", $time);
		$q->execute();
		$r = $q->fetch(PDO::FETCH_ASSOC);
		
		$durchlauf_id = $r['id'];
		
		return $durchlauf_id;
	}
	
	function saveAnswer($get, $post, $hash) {
		$dbh = new PDO(PDO, DBUSERNAME, DBPASSWORD);
		$antworten = explode("<br />", nl2br($this->data['antworten']));
        
        if ($get['e'] == 1) {
            $v = 'DER_NUTZER_HAT_SICH_ENTHALTEN';
            $q = $dbh->prepare("INSERT INTO `mvote_teilnahme` (`id`, `antwort`, `hash`, `durchlauf_id`) VALUES (NULL, :antwort, :hash, :durchlauf_id);");
            $q->bindParam(":antwort", $v);
            $q->bindParam(":durchlauf_id", $get['d']);
            $q->bindParam(":hash", $hash);
            $q->execute();
            return;
        }
        $c = 0;
		foreach ($antworten as $v=>$a) {
			$array = array(" ", ".");
			if ($get['check-'.md5(trim(str_replace($array, "_", $a)))] == "on") {
				$q = $dbh->prepare("INSERT INTO `mvote_teilnahme` (`id`, `antwort`, `hash`, `durchlauf_id`) VALUES (NULL, :antwort, :hash, :durchlauf_id);");
				$q->bindParam(":antwort", $v);
				$q->bindParam(":durchlauf_id", $get['d']);
				$q->bindParam(":hash", $hash);
				$q->execute();
                $c++;
			}
		}
        // save that nothing was selected.
        /*if ($c == 0) {
            	$q = $dbh->prepare("INSERT INTO `mvote_teilnahme` (`id`, `antwort`, `hash`, `durchlauf_id`) VALUES (NULL, :antwort, :hash, :durchlauf_id);");
                $null = -1;
				$q->bindParam(":antwort", $null);
				$q->bindParam(":durchlauf_id", $get['d']);
				$q->bindParam(":hash", $hash);
				$q->execute();
        }*/
	}
	
	function getPresentation() {
		if ($this->data['diagrammtyp'] == 0) {
			$color = 0;
			$antworten = explode("<br />", nl2br($this->data['antworten']));
			
			$desc = '<div style="width: 310px; left: 50px; position: absolute; top: 110px;">';
			
			$values = '{labels:[""], datasets : [';
			$c = 0;
			$max = 0;
			foreach ($antworten as $v=>$a) {
				$qa = $this->dbh->prepare("SELECT COUNT(*) as count FROM `mvote_durchlauf`, `mvote_teilnahme` WHERE mvote_durchlauf.id = :durchlauf_id AND mvote_durchlauf.id = mvote_teilnahme.durchlauf_id AND mvote_teilnahme.antwort = :antwort");
				$qa->bindParam(":durchlauf_id", $this->data['id']);
				$qa->bindParam(":antwort", $v);
				$qa->execute();
				$ra = $qa->fetch(PDO::FETCH_ASSOC);
				if ($c != 0)
					$values .= ",";
				$values .= "{ data: [".$ra['count']."], fillColor: '".getColor($color)."', strokeColor: '".getColor($color)."'}";
				$c++;
				
				$desc.="<div style='margin: 10px; text-align:left; font-size: 1.5em;'><div style='padding:15px; background:".getColor($color)."; float:left;'></div>".trim($antworten[$v])."<br>[".round($ra['count']*100/$this->data['count'])."%]</div>" ;
				$color++;
				if ($ra['count'] > $max) {
					$max = $ra['count'];
                }
                
			}
			$values .= "]};";
			$desc .= "</div>";
		
            // get number of unique devices
            $query_count = $this->dbh->prepare("SELECT hash FROM `mvote_teilnahme` WHERE durchlauf_id = :durchlauf_id GROUP by hash");
            $query_count->bindParam(":durchlauf_id", $this->data['id']);
            $query_count->execute();
            $result_count = $query_count->fetchAll(PDO::FETCH_ASSOC);
            $numberOfUniqueDevices = count($result_count);
	    $query_count_enthaltung = $this->dbh->prepare("SELECT hash FROM `mvote_teilnahme` WHERE durchlauf_id = :durchlauf_id GROUP by hash AND antwort = 'DER_NUTZER_HAT_SICH_ENTHALTEN'");
            $query_count_enthaltung->bindParam(":durchlauf_id", $this->data['id']);
            $query_count_enthaltung->execute();
            $result_count_enthaltung = $query_count_enthaltung->fetchAll(PDO::FETCH_ASSOC);
	    $numberOfEnthaltungen = count($result_count_enthaltung);
		
		$output = $desc . '<canvas id="diagramm" width="700" height="500"></canvas>';
	if ($this->data['enthaltung'] == 1) {
		// enthaltungen aktiv
		if ($this->data['mehrfachteilnahme'] == 0) {
		    $max = $numberOfUniqueDevices;
		    $output .= '<h2>'.$numberOfUniqueDevices.' Teilnehmer ('. $numberOfEnthaltungen .' Enthaltungen)</h2>';
		} else {
		    $output .= '<h2>'.$this->data['count'].' Stimmen ('. $numberOfEnthaltungen .' Enthaltungen)</h2>';
		}
	} else {
		if ($this->data['mehrfachteilnahme'] == 0) {
		    $max = $numberOfUniqueDevices;
		    $output .= '<h2>'.$numberOfUniqueDevices.' Teilnehmer</h2>';
		} else {
		    $output .= '<h2>'.$this->data['count'].' Stimmen</h2>';
		}
	}
        
	$output .= '
						<script>
	var data = '.$values.'
	
	var ctx = $("#diagramm").get(0).getContext("2d");
	var myNewChart = new Chart(ctx).Bar(data, {scaleOverride : true,scaleStepWidth : ';
	$stepWidth = round($max/10);
	if ($stepWidth == 0)
		$stepWidth = 1;
	$output .= $stepWidth.',scaleSteps : '.ceil($max/$stepWidth).', scaleStartValue : 0,
   
  });';
  
	/*
	Here this function is not working correctly
	$output .= 'var myVar = setInterval(function(){ ladeErgebnisse("'.strtoupper($_GET['id']).'",'.$this->data['id'].') }, 5000);
	setInterval(function(){ ladeErgebnisse("'.strtoupper($_GET['id']).'",'.$this->data['id'].')}, 5000);
	myStopFunction();
	function myStopFunction() {
	    clearInterval(myVar);	
}';
	*/
	$output .='</script>';
    // ChartNew.js: inGraphDataShow : true,
	return $output;
		}
		// Tortendiagramm
		if ($this->data['diagrammtyp'] == 1) {
			$color = 0;
		
			$values = "[";
			$antworten = explode("<br />", nl2br($this->data['antworten']));
			$c = 0;
			$desc .= '<div class="color_pres col-lg-2">';
			foreach ($antworten as $v=>$a) {
				$qa = $this->dbh->prepare("SELECT COUNT(*) as count FROM `mvote_durchlauf`, `mvote_teilnahme` WHERE mvote_durchlauf.id = :durchlauf_id AND mvote_durchlauf.id = mvote_teilnahme.durchlauf_id AND mvote_teilnahme.antwort = :antwort");
				$qa->bindParam(":durchlauf_id", $this->data['id']);
				$qa->bindParam(":antwort", $v);
				$qa->execute();
				$ra = $qa->fetch(PDO::FETCH_ASSOC);
				if ($c != 0)
					$values .= ",";
				$values .= "{ value: ".$ra['count'].", color: '".getColor($color)."'}";
				$c++;
				$desc.="<div style='margin: 10px; text-align:left; font-size: 1.5em;'><div style='padding:15px; background:".getColor($color)."; float:left;'></div>".trim($antworten[$v])."[".round($ra['count']*100/$this->data['count'])."%]</div>" ;
				$color++;
			}
			$values .= "]";
			$desc .= "</div>";
		
			$output = $desc.'<canvas id="diagramm" width="500" height="500"></canvas>';
			//<h2>'.$this->data['count'].' Stimmen</h2>
			
			
			$query_count_enthaltung = $this->dbh->prepare("SELECT hash FROM `mvote_teilnahme` WHERE durchlauf_id = :durchlauf_id GROUP by hash AND antwort = 'DER_NUTZER_HAT_SICH_ENTHALTEN'");
			$query_count_enthaltung->bindParam(":durchlauf_id", $this->data['id']);
			$query_count_enthaltung->execute();
			$result_count_enthaltung = $query_count_enthaltung->fetchAll(PDO::FETCH_ASSOC);
			$numberOfEnthaltungen = count($result_count_enthaltung);
			
			if ($this->data['enthaltung'] == 1) {
				$output .= '<h2>'.$this->data['count'].' Stimmen ('. $numberOfEnthaltungen .' Enthaltungen)</h2>';
			} else {
				$output .= '<h2>'.$this->data['count'].' Stimmen</h2>';
			}
			
			$output .= '
			<script>
			var data = '.$values.'
	
			var ctx = $("#diagramm").get(0).getContext("2d");
			var myNewChart = new Chart(ctx).Pie(data);
			';
	/*
	Here this function is not working correctly
	
	$output .= 'var myVar = setInterval(function(){ ladeErgebnisse("'.strtoupper($_GET['id']).'",'.$this->data['id'].') }, 5000);
	setInterval(function(){ ladeErgebnisse("'.strtoupper($_GET['id']).'",'.$this->data['id'].')}, 5000);
	myStopFunction();
	function myStopFunction() {
	    clearInterval(myVar);
	}
	';
	*/
            $output .='</script>';
			return $output;
		}
	}
	
}

?>
