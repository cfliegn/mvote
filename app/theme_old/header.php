<!DOCTYPE html> 
<html lang="de"> 
<head>
	<meta charset="UTF-8">
	<title>mVote</title> 
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<!--<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
	<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>-->
	<link rel="stylesheet" href="theme/mvote.min.css" />
	<link rel="stylesheet" href="theme/jquery.mobile.structure-1.2.0.min.css" />
	<script src="lib/jquery-1.8.3.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
	<style>
	.ui-controlgroup-controls {
		width: 100%!important;
	}
.greenbg {
	color: rgb(70, 136, 71);
	background-color: rgb(223, 240, 216);
	border-radius: 10px 10px 10px 10px;
	border-color: rgb(214, 233, 198);
}

.redbg {
	color: rgb(185, 74, 72);
	background-color: rgb(242, 222, 222);
	border-radius: 10px 10px 10px 10px;
	border-color: rgb(238, 211, 215);
}
.ui-li .ui-link-inherit { 
    white-space: normal !important; 
}
	</style>
</head> 
<body> 
